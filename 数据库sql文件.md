### book表

```sql
book表
CREATE TABLE `book` (
  `Book_id` int NOT NULL AUTO_INCREMENT COMMENT '书籍编号',
  `Book_name` varchar(255) DEFAULT NULL COMMENT '书籍名称',
  `Book_type` varchar(255) DEFAULT NULL COMMENT '书籍类型',
  `Author` varchar(255) DEFAULT NULL COMMENT '作者',
  `Publisher` varchar(255) DEFAULT NULL COMMENT '出版社',
  `Borrow_State` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '借阅状态',
  PRIMARY KEY (`Book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb3
```



### borrowrecord表

```sql
borrowrecord表
CREATE TABLE `borrowrecord` (
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `borrow_time` int DEFAULT NULL COMMENT '借阅时长',
  `when_begin` varchar(255) DEFAULT NULL COMMENT '从何时开始借阅（管理员同意审批时间）',
  `borrowbook_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '借阅书籍编号',
  `return_time` varchar(255) DEFAULT NULL COMMENT '归还时间',
  `return_book` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '归还书籍',
  `borrow_state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '借阅状态',
  `control_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审批类型',
  `control_state` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '审批状态',
  `submit_borrowtime` varchar(255) DEFAULT NULL COMMENT '提交借书审批时间',
  `control_borrowtime` varchar(255) DEFAULT NULL COMMENT '管理员反馈借书审批时间',
  `whether_agreeborrow` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否同意借书申请',
  `submit_returntime` varchar(255) DEFAULT NULL COMMENT '提交还书审批时间',
  `control_returntime` varchar(255) DEFAULT NULL COMMENT '管理员反馈归还审批时间',
  `whether_agreereturn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否同意还书申请',
  `submit_extendtime` varchar(255) DEFAULT NULL COMMENT '提交续期审批时间',
  `control_extendtime` varchar(255) DEFAULT NULL COMMENT '管理员反馈续期审批时间',
  `whether_agreeextend` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '是否同意续期申请',
  `borrowbook_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '借阅书籍名称',
  `id` int NOT NULL AUTO_INCREMENT COMMENT '自增',
  `extend_time` int DEFAULT NULL COMMENT '续期天数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 |
```



### user表

```sql
user表
CREATE TABLE `user` (
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `QQ_number` int DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `picture` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `nper` varchar(255) DEFAULT NULL,
  `direction` varchar(255) DEFAULT NULL,
  `student_id` int DEFAULT NULL,
  `professional_class` varchar(255) DEFAULT NULL,
  `campus` varchar(255) DEFAULT NULL,
  `registration_date` date DEFAULT NULL,
  `overdue_number` int DEFAULT NULL,
  `administrator` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `borrow_status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 |
```

