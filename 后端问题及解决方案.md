# 后端问题及解决方案

## 接收前端数据及传给前端数据

为保证数据类型统一，前后端数据类型都用JSON

接收前端数据：
我们所采用的方式：将前端传的数据用一个java对象接收转化为JSON对象，之后通过get，set方法操作所需数据

接收前端数据的类：

```java
public class Search {
 //属性的个数，类型及名称与前端传来的一致
    private String FindWay;
    private String inputcontent;

    public Search(){

    }

    public String getFindWay() {
        return FindWay;
    }

    public void setFindWay(String findWay) {
        FindWay = findWay;
    }

    public String getInputcontent() {
        return inputcontent;
    }

    public void setInputcontent(String inputcontent) {
        this.inputcontent = inputcontent;
    }
}
```

为保证代码统一我们又写了一个将JSON对象转化为java对象的类，其实这个可以不用 直接将代码写在接收数据之后也行

```java
public class JsonChangeSearch {
    public Search JsonChangeJavaObject(String str) {
        Search search = JSON.parseObject(str, Search.class);
        return search;
    }
}
```



```java
        //使用InputStreamReader对象，获取前端传来的数据
        // request.getInputStream()是读取前端传递来的数据字节流
        // StandardCharsets.UTF_8是将前端传来的数据转化为UTF-8的编码方式
        InputStreamReader insr = new InputStreamReader(req.getInputStream(), StandardCharsets.UTF_8);
        StringBuilder body = new StringBuilder();
        int respInt = insr.read();
        while(respInt!=-1) { 
            // 读取请求数据
            //将读取的字节流中的每一个字节转化为字符，然后添加到StringBuilder类型的对象中
            body.append((char) respInt);
            respInt = insr.read();
        }
		//将JSON对象转化为java对象
		JsonChangeSearch jsonChangeSearch = new JsonChangeSearch();
        //将StringBuilder类型的对象的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        Search search = jsonChangeSearch.JsonChangeJavaObject(body.toString());
		//没有写转化类可以直接用如下代码:
		/*将StringBuilder类型的对象body的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        Search search = JSON.parseObject(body.toString(),search.class);
        */
		//之后我们就可以操作前端传来的数据了：
        search.getFindWay();
		search.getInputcontent();
```

当然如果前端传来的不是json对象而是json格式的数据（类似键值对那样的）(到底用什么方式前期一定沟通好)

参考尔豪学长代码：

```java
//fastjson
InputStreamReader inputStreamReader = new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8);
            StringBuilder str = new StringBuilder();
            int respInt = inputStreamReader.read();
            while (respInt!=-1){
                str.append((char) respInt);
                respInt = inputStreamReader.read();
            }
//假设前端传过来的数据参数名叫str       
String jsonString =String.valueOf(str);
JsonObject jsonObject=JsonObject.parseObject(jsonString);
/*
 *获得数据：
 *jsonObject.getString();
 */
//数据转换成json字符串
//String jsonString02 = JsonObject.toJsonString(Object o);
```

后端返回数据：

由于考虑到传回数据较多，我们采用将java集合转化成JSON数据发送给前端

```java
//创建对象：
//注意重写toString()方法
public class ReturnSearch {
    private String user_name;
    private String password;
    private String phone_number;
    private String QQ_number;
    private String email;
    private String name;
    private String sex;
    private String nper;
    private String direction;
    private String student_id;
    private String professional_class;
    private String campus;
    private Date registration_date;
    private int overdue_number;
    private String administrator;
    private String borrow_status;

    public ReturnSearch(String user_name, String password, String phone_number, String QQ_number, String email, String name, String sex, String nper, String direction, String student_id, String professional_class, String campus, Date registration_date, int overdue_number, String administrator,  String borrow_status) {
        this.user_name = user_name;
        this.password = password;
        this.phone_number = phone_number;
        this.QQ_number = QQ_number;
        this.email = email;
        this.name = name;
        this.sex = sex;
        this.nper = nper;
        this.direction = direction;
        this.student_id = student_id;
        this.professional_class = professional_class;
        this.campus = campus;
        this.registration_date = registration_date;
        this.overdue_number = overdue_number;
        this.administrator = administrator;
        this.borrow_status = borrow_status;
    }
    public ReturnSearch(){

    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getQQ_number() {
        return QQ_number;
    }

    public void setQQ_number(String QQ_number) {
        this.QQ_number = QQ_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNper() {
        return nper;
    }

    public void setNper(String nper) {
        this.nper = nper;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getProfessional_class() {
        return professional_class;
    }

    public void setProfessional_class(String professional_class) {
        this.professional_class = professional_class;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public Date getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(Date registration_date) {
        this.registration_date = registration_date;
    }

    public int getOverdue_number() {
        return overdue_number;
    }

    public void setOverdue_number(int overdue_number) {
        this.overdue_number = overdue_number;
    }

    public String getAdministrator() {
        return administrator;
    }

    public void setAdministrator(String administrator) {
        this.administrator = administrator;
    }

    public String getBorrow_status() {
        return borrow_status;
    }

    public void setBorrow_status(String borrow_status) {
        this.borrow_status = borrow_status;
    }

    @Override
    public String toString() {
        return  "user_name='" + user_name + '\'' +
                ", password='" + password + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", QQ_number=" + QQ_number +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", nper='" + nper + '\'' +
                ", direction='" + direction + '\'' +
                ", student_id='" + student_id + '\'' +
                ", professional_class='" + professional_class + '\'' +
                ", campus='" + campus + '\'' +
                ", registration_date=" + registration_date +
                ", overdue_number=" + overdue_number +
                ", administrator='" + administrator + '\'' +
                ", borrow_status='" + borrow_status + '\'' ;
    }
}

```

```java
 List<ReturnSearch> list=new ArrayList<ReturnSearch>();
 list.add(new ReturnSearch(user_name,password,phone_number,QQ_number,email,name,sex,nper,direction,student_id,professional_class,campus,registration_date,overdue_number,administrator,borrow_status));
//java集合转换成json数据
String jsonObject= JSONObject.toJSONString(list);
out.println(jsonObject);
```

## 跨域问题

我们组直接用过滤器，不太懂，直接CV

```java
public class Filter implements javax.servlet.Filter {
    @Override
    public void destroy() {
    }
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        // 不使用*，自动适配跨域域名，避免携带Cookie时失效
        String origin = request.getHeader("Origin");
        response.setHeader("Access-Control-Allow-Origin", origin);
        // 自适应所有自定义头
        String headers = request.getHeader("Access-Control-Request-Headers");
        response.setHeader("Access-Control-Allow-Headers", headers);
        response.setHeader("Access-Control-Expose-Headers", headers);
        // 允许跨域的请求方法类型
        response.setHeader("Access-Control-Allow-Methods", "*");
        // 预检命令（OPTIONS）缓存时间，单位：秒
        response.setHeader("Access-Control-Max-Age", "3600");
        // 明确许可客户端发送Cookie，不允许删除字段即可
        response.setHeader("Access-Control-Allow-Credentials", "true");
        //参数 request, response 为web 容器或 Filter 链的上一个 Filter 传递过来的请求和相应对象；参数 chain 代表当前 Filter 链的对象。
        // 交给下一个过滤器或servlet处理
        chain.doFilter(request, response);
        //添加一个给后台的反馈
        System.out.println("过滤器已执行");
    }
    @Override
    public void init(FilterConfig config) throws ServletException {
    }
}
```

## 内网穿透问题不再赘述

## 判断逾期

我们的思路是先将本来要归还的时间和实际归还时间都转化为毫秒然后进行比较

```java
//首先将时间格式转化，由于我们提前沟通好所以这里没有进行转化
//SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        long curSysTime1 =0;
//可能会抛出异常将操作写在try...catch...中
                        try {
                            //同意借书审批的时间
                            // formatter.parse()方法可以将时间转化为毫秒
                            curSysTime1 = formatter.parse(control_borrowtime).getTime();
                            //本来需要还书的时间
                            long temp=(long)borrow_time;
                            long deadtime=curSysTime1+temp*24*60*60*1000;
                            //提交还书审批的时间
                            long   curSysTime2 = 0;
                            curSysTime2 = formatter.parse(submit_returntime).getTime();
                            if (curSysTime2>deadtime){
                                String sql4 = "SELECT * FROM user WHERE user_name='" + control2Class.getId() + "'";
                                ResultSet rs2 = stmt.executeQuery(sql4);
                                while (rs2.next()){
                                    int overdue_number = rs2.getInt("overdue_number");
                                    int overdue_number2=overdue_number+1;
                                    String sql5 = "UPDATE user SET overdue_number='"+overdue_number2+"'  WHERE user_name='"+control2Class.getId()+"'";
                                    stmt.executeUpdate(sql5);
                                }
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
```

#### 遇到的问题:

​		邮箱登录需要让其跳过密码判断，直接登录。但是前端会传过来一个密码为空的字符串，与数据库中的密码比较，因此一直显示登录失败。

#### 解决的办法:

​		将邮箱登录与其他需要密码的登录方式分开，用 if，else if 语句让其只能选择一个执行，即邮箱登录时跳过密码验证。

#### 遇到的问题: 

​		检测邮箱是否已经注册，查询数据库后，若结果为空，则返回"该邮箱未注册！"

```java
if (rs.next()==null)
if (rs==null)
if (rs.equals(null))
```

​		这些都是错误的表示方法

#### 解决的办法: 

​		正确的表示方法：

```java
if (!rs.next())
```

#### 遇到的问题: 

​		在将注册信息写入数据库时，要根据条件防止重复插入。规定用户名，电话号码，邮箱号码都需要保证唯一。

#### 解决的办法: 

​		1.在插入数据库前先查询用户名，电话号码，邮箱号码是否有相同的，若有相同的则返回注册失败即失败的原因。

​		2.数据库根据条件防止重复插入： INSERT INTO IF EXISTS。

NOT EXISTS后面的sql语句，如果能查出值，则不影响行为：否则会执行插入语句。



#### 遇到的问题: 

​		邮箱验证需要一个随机验证码

#### 解决的办法: 

```java
String str="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
StringBuilder sb=new StringBuilder(4);
for(int i=0;i<4;i++){
char ch=str.charAt(newRandom().nextInt(str.length()));
sb.append(ch);
}
```



