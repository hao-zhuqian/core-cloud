//获取当前时间函数
axios.defaults.baseURL='http://xinyun.natapp1.cc';
function getNowDateAfterFormat() {
    let date = new Date();
    let year = date.getFullYear().toString().padStart(4, "0"),
        month = (date.getMonth() + 1).toString().padStart(2, "0"),
        day = date.getDate().toString().padStart(2, "0"),
        hour = date.getHours().toString().padStart(2, "0"),
        min = date.getMinutes().toString().padStart(2, "0"),
        sec = date.getSeconds().toString().padStart(2, "0");
    return year+'-'+month+'-'+day+' '+hour+':'+min+':'+sec;
}
//获取书籍编号
function bornum(){
    let num = $("#number").val()
    return num
}
//获取书籍名称
function borname(){
    let name = $("#book").val()
    return name
}
//获取借阅时长
function bortime(){
    let time = $("#times").val()
    return time
}
//绑定借书事件
let submit = document.getElementById("submit")
submit.onclick = function(){
    axios({
        method:"POST",
        url:"/borrowbook",
        data:{
            applyforname:localStorage.getItem('user_name'),
            applyfornumber:bornum(),
            applyforbook:borname(),
            applyfortime:bortime(),
            time:getNowDateAfterFormat()
        }
    })
    .then(function(response){
        console.log(response);
		if(response.data=="失败")
			{alert("书籍编号与书名不匹配")}
		else if(response.data=="图书已被借阅")
			{alert("图书已被借阅")}
		else
		{
			alert("提交借书申请成功")
			$("#number").val("")
			$("#book").val("")
			$("#times").val("")
		}
    })
    .catch(function(error){
        console.log(error);
        alert("出错啦！");
    })
}


