axios.defaults.baseURL='http://xinyun.natapp1.cc';
localStorage.clear();
var show_num = [];

$(function(){
  draw();
  $("#canvas").on('click',function(){draw(show_num);})
  $(".btn").on('click',function(){
    let name=document.getElementById('name').value;
		let password=document.getElementById('password').value;
		let user={
    	user_message:name,
    	password:password,
    	searchway:'user_name',
		}
    var val = $(".input-val").val().toLowerCase();
    var num = show_num.join("");
    if(val=='')
    	{alert('请输入验证码！');}
    else if(val == num){
			axios({
				method:'POST',
				url:'/login',
				data:JSON.stringify(user),
			})
			.then(response=>{
				console.log(response.data)
				if(response.data=='密码错误'||response.data=='登录信息不存在'||response.data=='登录失败')
				{
					console.log(response.data);
					alert(response.data)
				}
        else{
          console.log(response)
          //是否为管理员
          localStorage.setItem('administrator', response.data.administrator);
          //方向
          localStorage.setItem('direction', response.data.direction);
          //姓名
          localStorage.setItem('name', response.data.name);
          //逾期次数
          localStorage.setItem('overdue_number', response.data.overdue_number);
          //专业班级
          localStorage.setItem('professional_class', response.data.professional_class);
          //用户名
          localStorage.setItem('user_name', response.data.user_name);
          //邮箱
          localStorage.setItem('email', response.data.email);
          //密码
          localStorage.setItem('password', password);
          //校区
          localStorage.setItem('campus', response.data.campus);
          //期数
          localStorage.setItem('nper', response.data.nper);
          //电话
          localStorage.setItem('phone_number', response.data.phone_number);
          //头像
          localStorage.setItem('picture', response.data.picture);
          //学号
          localStorage.setItem('student_id', response.data.student_id);
          //性别
          localStorage.setItem('sex', response.data.sex);
          //注册时间
          localStorage.setItem('registration_date', response.data.registration_date);
          //QQ
          localStorage.setItem('qQ_number', response.data.qQ_number);
          location.href="start.html";
        }
			})
    }
    else{
      alert('验证码错误！请重新输入！');
      alert(num);
      $(".input-val").val('');
      draw(show_num);
    }
  })
})

    //生成并渲染出验证码图形
    function draw() {
        var canvas_width=$('#canvas').width();
        var canvas_height=$('#canvas').height();
        var canvas = document.getElementById("canvas");//获取到canvas的对象，演员
        var context = canvas.getContext("2d");//获取到canvas画图的环境，演员表演的舞台
        canvas.width = canvas_width;
        canvas.height = canvas_height;
        var sCode = "a,b,c,d,e,f,g,h,i,j,k,m,n,p,q,r,s,t,u,v,w,x,y,z,A,B,C,E,F,G,H,J,K,L,M,N,P,Q,R,S,T,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0";
        var aCode = sCode.split(",");
        var aLength = aCode.length;//获取到数组的长度
        
        for (var i = 0; i < 4; i++) {  //这里的for循环可以控制验证码位数（如果想显示6位数，4改成6即可）
            var j = Math.floor(Math.random() * aLength);//获取到随机的索引值
            // var deg = Math.random() * 30 * Math.PI / 180;//产生0~30之间的随机弧度
            var deg = Math.random() - 0.5; //产生一个随机弧度
            var txt = aCode[j];//得到随机的一个内容
            show_num[i] = txt.toLowerCase();
            var x = 10 + i * 20;//文字在canvas上的x坐标
            var y = 20 + Math.random() * 8;//文字在canvas上的y坐标
            context.font = "bold 23px 微软雅黑";

            context.translate(x, y);
            context.rotate(deg);

            context.fillStyle = randomColor();
            context.fillText(txt, 0, 0);

            context.rotate(-deg);
            context.translate(-x, -y);
        }
        for (var i = 0; i <= 5; i++) { //验证码上显示线条
            context.strokeStyle = randomColor();
            context.beginPath();
            context.moveTo(Math.random() * canvas_width, Math.random() * canvas_height);
            context.lineTo(Math.random() * canvas_width, Math.random() * canvas_height);
            context.stroke();
        }
        for (var i = 0; i <= 30; i++) { //验证码上显示小点
            context.strokeStyle = randomColor();
            context.beginPath();
            var x = Math.random() * canvas_width;
            var y = Math.random() * canvas_height;
            context.moveTo(x, y);
            context.lineTo(x + 1, y + 1);
            context.stroke();
        }
    }

    //得到随机的颜色值
    function randomColor() {
        var r = Math.floor(Math.random() * 256);
        var g = Math.floor(Math.random() * 256);
        var b = Math.floor(Math.random() * 256);
        return "rgb(" + r + "," + g + "," + b + ")";
    }