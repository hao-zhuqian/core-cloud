//判断查询方式即FindWays的值
axios.defaults.baseURL='http://xinyun.natapp1.cc';
let FindWays="Book_name"
let way
let ddd=document.getElementById("add");
let aaa=document.getElementsByClassName("aaa");
let chos=document.getElementsByClassName("chos");
chos[0].style.backgroundColor="rgba(40,60,100,1)"

//显示选择方式选中效果
for(let i=0;i<chos.length;i++)
{
	chos[i].onclick=function(){
    	clos();
    	chos[i].style.backgroundColor="rgba(40,60,100,1)"
    }
}
function clos(){
	for(let i=0;i<chos.length;i++)
    {
    	chos[i].style.backgroundColor="rgba(255,255,255,0)"
    }
}
//加载页面时展示未借阅书籍
$(document).ready(function(){
    axios({
        method:"POST",
        url:"/userbook",
        data:{
            FindWays:"Borrow_State",
            input:"未借阅"  
        }
    })
    .then(function(response){
    	//若后端未搜索到则返回结果'[]\n失败'此时清空搜索结果
    	if(response =='[]\n失败')
    		{definite.innerHTML=""}
    	//对后端返回结果逐条渲染
    	else
    	{
        	console.log(response)
        	//清空原有数据
        	definite.innerHTML = ""
        	//渲染书籍信息
			if(response.data!="")
			{
        		let data = response.data
        		//若不是管理员则渲染时不显示书籍删除按钮
        		for(let i=0;i<data.length;i++)
        		{
            		if(localStorage.getItem('administrator')=="否")
            		{
            			definite.innerHTML = definite.innerHTML + `<div class="definite">
			            <div class="infor1">${data[i].book_id}</div>
			            <div class="infor2">${data[i].book_name}</div>
			            <div class="infor">${data[i].author}</div>
			            <div class="infor">${data[i].book_type}</div>
			            <div class="infor">${data[i].publisher}</div>
			            <div class="infor">${data[i].borrow_state}</div></div>`
						aaa[0].style.display="none"
            		}
            		else
            		{
		            	definite.innerHTML = definite.innerHTML + `<div class="definite">
			            <div class="infor1">${data[i].book_id}</div>
			            <div class="infor2">${data[i].book_name}</div>
			            <div class="infor">${data[i].author}</div>
			            <div class="infor">${data[i].book_type}</div>
			            <div class="infor">${data[i].publisher}</div>
			            <div class="infor">${data[i].borrow_state}</div>
			            <button class="reduce" id="reduce">删除</button></div>`
		            	aaa[0].style.display="block"
	            	}
            	}
        		//获取删除按键与编号与书名以及对应信息
				let reduce = document.getElementsByClassName("reduce")
				let infor1 = document.getElementsByClassName("infor1")
				let infor2 = document.getElementsByClassName("infor2")
				let def = document.getElementsByClassName("definite")
				//为所有删除按钮绑定事件
			    for(let i=0;i<reduce.length;i++)
			    {
			        reduce[i].index = i;
			        reduce[i].onclick=function(){
			        	//假删效果
			          	def[this.index].style.display="none"
			          	//向后端发起删除申请
            			axios({
                			method:"POST",
                			url:"/reducemangerbook",
                			data:{
                    				booknames:infor2[this.index+1].innerText,
                    				id:infor1[this.index+1].innerText
                			}
                		})
                		.then(function(response){
                    		console.log(response)
                    	})
            			.catch(function(error){
                    		console.log(error)
                    		alert("出错啦")
                		})
            		}//删除按钮点击事件的终止符
         		}//为所有删除按钮绑定事件的终止符
			}//渲染书籍信息终止符
        }//对后端返回结果逐条渲染终止符
    })//加载页面时展示未借阅书籍函数终止符
})//加载页面时展示未借阅书籍事件终止符
//普通用户隐藏书籍添加与删除按钮
if(localStorage.getItem('administrator')=="否")
{
	ddd.style.display="none"
	aaa[0].style.display="none"
}
//根据用户搜索类型不同修改参数
$( function(){$(".chos").click(function(){
        let way = $(this).html()
        if(way == "书籍名称")
        	{FindWays ="Book_name"}
        if(way == "书籍类别")
        	{FindWays="Book_type"}
        if(way == "借阅状态")
        	{FindWays="Borrow_State"}
        console.log(FindWays)
    })
})
//获取查询方式
function findway()
	{return FindWays}
//获取输入的内容
function content(){
	let content = $("#content").val()
	return content
}
//获取搜索按键
let search = document.getElementById("search")
let definite = document.getElementById("DefiniteList")
//搜索按钮绑定事件
search.onclick = function(){
    axios({
        method:"POST",
        url:"/userbook",
        data:{
            FindWays:findway(),
            input:content()  
        }
    })
    .then(function(response){
    	if(response.data=="[]\n失败")
    		{definite.innerHTML=""}
    	else
    	{
        	console.log(response)
        	let data = response.data
        	definite.innerHTML = ""
        	for(let i=0;i<data.length;i++)
        	{
            	if(localStorage.getItem('administrator')=="否")
            	{
	            	definite.innerHTML = definite.innerHTML + `<div class="definite">
		            <div class="infor1">${data[i].book_id}</div>
		            <div class="infor2">${data[i].book_name}</div>
		            <div class="infor">${data[i].author}</div>
		            <div class="infor">${data[i].book_type}</div>
		            <div class="infor">${data[i].publisher}</div>
		            <div class="infor">${data[i].borrow_state}</div></div>`
					aaa[0].style.display="none"
	            }
	            else
	            {
	            	definite.innerHTML = definite.innerHTML + `<div class="definite">
		            <div class="infor1">${data[i].book_id}</div>
		            <div class="infor2">${data[i].book_name}</div>
		            <div class="infor">${data[i].author}</div>
		            <div class="infor">${data[i].book_type}</div>
		            <div class="infor">${data[i].publisher}</div>
		            <div class="infor">${data[i].borrow_state}</div>
		            <button class="reduce" id="reduce">删除</button></div>`
	            	aaa[0].style.display="block"
	            }
           	}
			let reduce = document.getElementsByClassName("reduce")
			let infor1 = document.getElementsByClassName("infor1")
			let infor2 = document.getElementsByClassName("infor2")
			let def
		    for(let i=0;i<reduce.length;i++)
		    {
		        reduce[i].index = i;
		        def = document.getElementsByClassName("definite")
		        reduce[i].onclick=function(){
		            console.log(infor1[this.index+1].innerText)
		            console.log(infor2[this.index+1].innerText)
		          	def[this.index].style.display="none"
		        	let s = {
		            booknames:infor2[this.index].innerText,
		            id:infor1[this.index].innerText
		        	}
		        	axios({
               			method:"POST",
                		url:"/reducemangerbook",
                		data:{
                    	booknames:infor2[this.index+1].innerText,
                    	id:infor1[this.index+1].innerText
                		}
              		})
                	.then(function(response){
                    	console.log(response)
                	})
            		.catch(function(error){
                    	console.log(error)
                    	alert("出错啦")
                	})
            	}
         	}
		}
    })   
}
//添加书籍
let add = document.getElementById("add")
let aa = document.getElementById("a")
add.onclick = function(){
    aa.style.display = "block"
 
}
let putin = document.getElementById("putin")
putin.onclick = function(){
    aa.style.display = "none"
    let addname = $("#name2").val()
    let addtype = $("#type2").val()
    let addauthor = $("#author2").val()
    let addpress = $("#press2").val()
    if(addname=="")
    	{alert("书名不能为空")}
    else if(addtype=="")
    	{alert("书籍类型不能为空")}
    else if(addauthor=="")
    	{alert("作者不能为空")}
    else if(addname=="")
    	{alert("出版社不能为空")}
    else 
    {
	 	axios({
	        method:"POST",
	        url:"/addmangerbook",
	        data:{
	           bookname:addname,
	           bookform:addtype,
	           bookauthor:addauthor,
	           bookpress:addpress
	        }
	   	})
	    .then(function(response){
	        console.log(response);
	    })
	    .catch(function(error){
	        console.log(error);
	        alert("出错啦！");
	    })
	}
}
let quit=document.getElementById("quit");
	quit.onclick = function(){
	aa.style.display = "none"
}



