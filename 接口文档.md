# 图书借阅功能Servlet

- **Description：**图书借阅功能相关API
- **Team:**  芯云
- **UrlPrefix:** 127.0.0.0:8080



## 一、登录

- **url：**/login
- **method Name**：Login（）
- **Description：**用户登录接口
- **Note：**通过登录信息和密码实现用户登录功能

### 1、Request Param

- **请求方法**：`POST`

| paramName | Type   | Required | Description                                                 |
| --------- | ------ | -------- | ----------------------------------------------------------- |
| searchway | String | True     | 登录方式（ QQ_number   phone_number   user_name   email  ） |
| name      | String | True     | 登录信息                                                    |
| password  | String | True     | 登录密码                                                    |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json
{
    "message":{
    	"administrator": "",
    	"borrow_status": "",
   	    "campus": "明向校区",
 	    "direction": "Java方向",
	    "email": "1776557437@qq.com",
    	"name": "dawangba",
	    "nper": "六期",
    	"overdue_number": 0,
    	"password": "123456",
    	"phone_number": "18135184737",
    	"professional_class": "软件2133",
    	"QQ_number": 1776557437,
    	"registration_date": "2022-02-20",
    	"sex": "女",
    	"student_id": 2021006089,
   		"user_name": "刘媛媛123"
	}（需传给个人界面）
    "message": "G9A5"
    "message": "登录成功"
}
```

#### -> Failure

```json
{
    massage.user_name: "searchway错误"
	massage.user_name: "登录信息不存在"
	"message": "登录失败，密码错误"
}
```



## 二、注册

- **url：**/Register
- **method Name**：Register（）
- **Description：**用户注册接口
- **Note：**用户填写个人信息实现注册功能

### 1、Request Param

- **请求方法**：`POST`

| paramName          | Type   | Required | Description        |
| ------------------ | ------ | -------- | ------------------ |
| changeway          | String | True     | register 或 delete |
| user_name          | String | True     | 用户名             |
| password           | String | True     | 密码               |
| phone_number       | String | True     | 电话号码           |
| QQ_number          | int    | True     | QQ账号             |
| email              | String | True     | 电子邮箱           |
| picture            | String | True     | 头像               |
| name               | String | True     | 名字               |
| sex                | String | True     | 性别               |
| nper               | String | True     | 期数               |
| direction          | String | True     | 方向               |
| student_id         | int    | True     | 学号               |
| professional_class | String | True     | 专业班级           |
| campus             | String | True     | 校区               |
| registration_date  | Date   | True     | 注册日期           |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json
{
    "message": "注册成功"
}
```

#### -> Failure

```json
{
	"message": "该用户名已存在"
	"message": "该电话号码已注册"
	"message": "注册失败"
}
```



## 三、注销

- **url：**/Delete
- **method Name**：Delete（）
- **Description：**用户注销账号接口
- **Note：**通过用户名实现注销用户账号功能

### 1、Request Param

- **请求方法**：`POST`

| paramName | Type   | Required | Description |
| --------- | ------ | -------- | ----------- |
| name      | String | True     | 注销信息    |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json
{
    massage.user_name: "注销成功"
}
```

#### -> Failure

```json
{
    massage.user_name: "searchway错误"
}
```



## 四、验证邮箱

- **url：**/ValidationRegister
- **method Name**：ValidationRegister（）
- **Description：**验证邮箱接口
- **Note：**通过发送邮箱验证码实现邮箱验证功能

### 1、Request Param

- **请求方法**：`POST`

| paramName   | Type   | Required | Description               |
| ----------- | ------ | -------- | ------------------------- |
| registerway | String | True     | 验证方式（现仅支持email） |
| useremail   | String | True     | 用户邮箱                  |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json
    "message": "ED3P"
```

#### -> Failure

```json

```



## 五、修改信息

- **url：**/ChangeMessage
- **method Name**：ChangeMessage()
- **Description：**用户修改个人信息接口
- **Note：**通过某一信息找到该用户实现修改个人信息功能

### 1、Request Param

- **请求方法**：`POST`

| paramName     | Type   | Required | Description |
| ------------- | ------ | -------- | ----------- |
| useclass      | String | True     | 验证信息类  |
| usemessage    | String | True     | 验证信息    |
| changeclass   | String | True     | 更改信息类  |
| changemessage | String | True     | 更改信息    |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json
    "message": "修改成功"
```

#### -> Failure

```json
   "message": "修改失败"
```



## 六、借阅记录

- **url：**/borrowhistory
- **method Name**：BorrowHistory ()
- **Description：**查询个人借阅记录接口
- **Note：**通过用户名找到该用户的所有个人借阅记录

### 1、Request Param

- **请求方法**：`POST`

| paramName | Type   | Required | Description |
| --------- | ------ | -------- | ----------- |
| user_name | String | True     | 用户名      |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json
{
    
	"BorrowhistoryClass":[
		{
			"book"(书名):[
			"《高等数学》",
			"《线性代数》",
			"《随便什么书名》",
			...
			],
			"InitiateTime"(申请发起时间):"2022-02-16 16:09:13",
			"Time"(申请时长):"12"(单位默认为天),
			"state":"已归还"
		},
		{
			"book"(书名):[
			"《高等数学》",
			"《线性代数》",
			"《随便什么书名》",
			...
			],
			"InitiateTime"(申请发起时间):"2022-02-16 16:09:13",
			"Time"(申请时长):"12"(单位默认为天),
			"state":"已归还"
		},
		...
	],
}
```

#### -> Failure

```json
	"message": "失败"
```



## 七、图书查询

- **url：**/userbook
- **method Name**：Userbook（）
- **Description：**书籍查询接口
- **Note：**通过各种条件查询列出书籍信息

### 1、Request Param

- **请求方法**：`POST`

| paramName | Type   | Required | Description                                                  |
| --------- | ------ | -------- | ------------------------------------------------------------ |
| FindWays  | String | True     | Borrow_State(借阅状态）,Book_type(类型)，Author(作者)，Book_name(书籍名称) |
| input     | String | True     | 用户查询时所输入的内容                                       |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json
{
	"data":[
		{
			"Book_id"(书籍编号):"1",
			"Book_name"(书名):"百年孤独",
			"Book_type"(书籍类型):"小说",
			"Author"(作者):"加夫列尔·加西亚·马尔克斯",
			"Publisher"(出版社):"南海出版社",
			"Borrow_state"(借阅状态：已借阅/未借阅):"已借阅"
		},
		{
			"Book_id"(书籍编号):"2",
			"Book_name"(书名):"百年孤独",
			"Book_type"(书籍类型):"小说",
			"Author"(作者):"加夫列尔·加西亚·马尔克斯",
			"Publisher"(出版社):"南海出版社",
			"Borrow_state"(借阅状态：已借阅/未借阅):"未借阅"
		},
		...
	],
}
```

#### -> Failure

```json
	"message": "失败"
```



## 八、图书借阅

- **url：**/borrowbook
- **method Name**：BorrowServlet（）
- **Description：**用户提交借阅书籍申请接口
- **Note：**用户填写借阅信息提交申请

### 1、Request Param

- **请求方法**：`POST`

| paramName      | Type   | Required | Description    |
| -------------- | ------ | -------- | -------------- |
| applyforname   | String | True     | 借书人         |
| applyforbook   | String | True     | 所借书籍名称   |
| applyfornumber | String | True     | 所借书籍编号   |
| applyfortime   | String | True     | 借阅时长       |
| time           | String | True     | 提交申请的时间 |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json

```

#### -> Failure

```json
"message": "失败"
```



## 九、图书归还

- **url：**/returnbook
- **method Name**：ReturnServlet（）
- **Description：**用户提交归还书籍申请接口
- **Note：**用户填写归还信息提交申请

### 1、Request Param

- **请求方法**：`POST`

| paramName    | Type   | Required | Description    |
| ------------ | ------ | -------- | -------------- |
| returnname   | String | True     | 还书人         |
| returnbook   | String | True     | 所还书籍名称   |
| returnnumber | String | True     | 所还书籍编号   |
| times        | String | True     | 提交申请的时间 |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json

```

#### -> Failure

```json
"message": "失败"
```



## 十、图书续期

- **url：**/returnbook
- **method Name**：ReturnServlet（）
- **Description：**用户提交图书续期申请接口
- **Note：**用户填写续期信息提交申请

### 1、Request Param

- **请求方法**：`POST`

| paramName    | Type   | Required | Description    |
| ------------ | ------ | -------- | -------------- |
| extendname   | String | True     | 借书人         |
| extendbook   | String | True     | 续借书籍名称   |
| extendnumber | String | True     | 续借书籍编号   |
| extendtime   | int    | True     | 续借时长       |
| timess       | String | True     | 提交申请的时间 |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json

```

#### -> Failure

```json
"message": "失败"
```



## 十一、管理员查询

- **url：**/search
- **method Name**：ControlSearch（）
- **Description：**管理员查询信息接口
- **Note：**管理员查找信息

### 1、Request Param

- **请求方法**：`POST`

| paramName    | Type   | Required | Description                                                  |
| ------------ | ------ | -------- | ------------------------------------------------------------ |
| FindWay      | String | True     | name(按姓名查询) ，direction(按方向查询)，professional_class(按专业班级查询)，campus（按校区查询），nper(按期数查询)，sex(按性别查询) |
| inputcontent | String | True     | 查询时输入的内容                                             |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json
{
	"data": [
		{
			"user_name"(用户名):"李",
			"password"(密码):"asd123",
			"phone_number"(电话):"18735074891",
			"QQ_number"(QQ号):"2781325001",
			"email"(邮箱):"2781325001@qq.com",
			"name"(姓名):"李菁菁",
			"sex"(性别):"女",
			"nper"(期数：例：六期):"六期",
			"direction"(方向：例：Java):"全栈",
			"student_id"(学号):"2021005442",
		},
			"professional_class"(专业班级：例：软件2104):"软件2112",
			"campus"(校区):"明向校区",
			"registration_date"(注册时间):"2021-02-19",
			"overdue_number"(逾期次数):"2",
			"administrator"(是否为管理员):"否",
			"borrow_states"(借阅状态):"已借阅"
		},	
		{
			"user_name"(用户名):"李",
			"password"(密码):"asd123",
			"phone_number"(电话):"18735074891",
			"QQ_number"(QQ号):"2781325001",
			"email"(邮箱):"2781325001@qq.com",
			"name"(姓名):"李菁菁",
			"sex"(性别):"女",
			"nper"(期数：例：六期):"六期",
			"direction"(方向：例：Java):"全栈",
			"student_id"(学号):"2021005442",
		},
			"professional_class"(专业班级：例：软件2104):"软件2112",
			"campus"(校区):"明向校区",
			"registration_date"(注册时间):"2021-02-19",
			"overdue_number"(逾期次数):"2",
			"administrator"(是否为管理员):"否",
			"borrow_states"(借阅状态):"已借阅"
		},
		...
	],	
}
```

#### -> Failure

```json
	"message": "失败"
```



## 十二、添加书籍

- **url：**/addmangerbook
- **method Name**：Addmangerbook（）
- **Description：**管理员添加书籍接口
- **Note：**管理员填写书籍信息添加书籍

### 1、Request Param

- **请求方法**：`POST`

| paramName  | Type   | Required | Description        |
| ---------- | ------ | -------- | ------------------ |
| bookname   | string | True     | 所添加书籍的名称   |
| bookform   | string | True     | 所添加书籍的类型   |
| bookauthor | string | True     | 所添加书籍的作者   |
| bookpress  | string | True     | 所添加书籍的出版社 |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json

```

#### -> Failure

```json
	"message": "失败"
```



## 十三、删除书籍

- **url：**/reducemangerbook
- **method Name**：ReduceMangerBook（）
- **Description：**管理员删除书籍接口
- **Note：**管理员填写书籍信息删除书籍

### 1、Request Param

- **请求方法**：`POST`

| paramName   | Type   | Required | Description      |
| ----------- | ------ | -------- | ---------------- |
| booknames   | string | True     | 所删除书籍的名称 |
| booknumbers | string | True     | 所删除书籍的编号 |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json

```

#### -> Failure

```json
"message": "失败"
```



## 十四、审批信息

- **url：**/control
- **method Name**：Control（）
- **Description：**管理员请求审批信息接口
- **Note：**管理员请求审批信息

### 1、Request Param

- **请求方法**：`POST`

| paramName | Type   | Required | Description |
| --------- | ------ | -------- | ----------- |
| Type      | String | True     | 请求类型    |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json
{
	"data": [
		{
			"InitiateTime"(申请发起时间):"2022-02-16 16:09:13",
			"Book"(书名):"高等数学",
			"Time"(申请时长):"12"(单位默认为天),
			"Id"(申请人id):"hth801219",
			"InitiateType":(审批类型),
			"Number":(审批编号)
		},
		{
			"InitiateTime"(申请发起时间):"2022-02-16 16:09:13",
			"Book"(书名):"高等数学",
			"Time"(申请时长):"12"(单位默认为天),
			(如果申请类型为还书申请则申请时长为0)
			"Id"(申请人id):"hth801219",
			"InitiateType":(审批类型),
			"Number":(审批编号)
		},
		...
	],
}
```

#### -> Failure

```json
	"message": "失败"
```



## 十五、执行审批

- **url：**/control2
- **method Name**：Control2（）
- **Description：**管理员执行审批接口
- **Note：**管理员执行审批，发送审批结果

### 1、Request Param

- **请求方法**：`POST`

| paramName    | Type    | Required | Description |
| ------------ | ------- | -------- | ----------- |
| Number       | int     | True     | 审批编号    |
| Id           | string  | True     | 用户名      |
| Pass         | Boolean | True     | 是否通过    |
| AuditTime    | string  | True     | 审批时间    |
| InitiateType | string  | True     | 审批类型    |

### 2、Response Param

- **返回值类型：**json

#### -> Success

```json

```

#### -> Failure

```json
    "message": "失败"
```

