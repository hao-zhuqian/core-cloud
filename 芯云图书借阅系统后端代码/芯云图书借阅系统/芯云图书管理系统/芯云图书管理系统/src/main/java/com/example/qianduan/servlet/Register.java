package com.example.qianduan.servlet;
import com.alibaba.fastjson.JSONObject;
import com.example.qianduan.jsonchangeclass.JsonChangeLiu;
import com.example.qianduan.method.RegisterDatabase;
import com.example.qianduan.changeclass.RegisterClass;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

/**
 * 注册功能
 * @author：刘媛媛
 */
@WebServlet(name = "Register", value = "/Register")
public class Register extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
// 响应参数格式设置
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        InputStreamReader insr = new InputStreamReader(req.getInputStream(), StandardCharsets.UTF_8);
        StringBuilder body = new StringBuilder();
        int respInt = insr.read();
        while (respInt != -1) {
            // 读取请求数据,将读取的字节流中的每一个字节转化为字符，然后添加到StringBuilder类型的对象中
            body.append((char) respInt);
            respInt = insr.read();
        }
        JsonChangeLiu jsonChangeLiu = new JsonChangeLiu();
        //将StringBuilder类型的对象的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        RegisterClass registerClass = jsonChangeLiu.JsonChangeJavaObject(body.toString());
        RegisterDatabase registerDatabase=new RegisterDatabase();
        String jsonobject= JSONObject.toJSONString(registerDatabase.ADdatabase(registerClass.getChangeway(),registerClass));
        out.println(jsonobject);
        System.out.println(registerClass);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}