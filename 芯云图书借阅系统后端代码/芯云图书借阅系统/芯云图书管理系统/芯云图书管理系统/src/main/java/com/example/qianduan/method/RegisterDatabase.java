package com.example.qianduan.method;

import com.example.qianduan.changeclass.RegisterClass;

import java.sql.*;

/**
 * 注册成功后往数据库中添加数据
 * @author：刘媛媛
 */
public class RegisterDatabase {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL ="jdbc:mysql://localhost:3306/demo?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "123456";

    public String ADdatabase(String changeway, RegisterClass register) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");
            stmt = conn.createStatement();
                String User_name = String.valueOf(register.getUser_name());
                String Password = String.valueOf(register.getPassword());
                String Phone_number = String.valueOf(register.getPhone_number());
                String QQ_number =String.valueOf(register.getQQ_number());
                String Email = String.valueOf(register.getEmail());
                String Picture = String.valueOf(register.getPicture());
                String Name = String.valueOf(register.getName());
                String Sex = String.valueOf(register.getSex());
                String Nper = String.valueOf(register.getNper());
                String Student_id = String.valueOf(register.getStudent_id());
                String Direction = String.valueOf(register.getDirection());
                String Professional_class = String.valueOf(register.getProfessional_class());
                String Campus = String.valueOf(register.getCampus());
                String Registration_date = String.valueOf(register.getRegistration_date());
                String sql = "SELECT * FROM user WHERE user_name='" + User_name + "'";
                ResultSet rs = stmt.executeQuery(sql);
                while (rs.next()) {
                    if (rs.getString("user_name").equals(User_name)) {
                        return "该用户名已存在";}}
                sql = "SELECT * FROM user WHERE phone_number='" + Phone_number + "'";
                rs = stmt.executeQuery(sql);
                while (rs.next()) {
                    if (rs.getString("phone_number").equals(Phone_number)) {
                        return "该电话号码已注册";}}
                sql = "INSERT INTO user(user_name,password,phone_number,QQ_number,email,name,sex,nper,direction,student_id,professional_class,campus,registration_date,picture,overdue_number,administrator,borrow_status) SELECT '" + User_name + "','" + Password + "','" + Phone_number + "','" + QQ_number + "','" + Email + "','" + Name + "','" + Sex + "','" + Nper + "','" + Direction + "','" + Student_id + "','" + Professional_class + "','" + Campus + "','" + Registration_date + "','" + Picture + "',0,'否','' FROM dual WHERE NOT EXISTS ( SELECT user_name from user WHERE phone_number='" + Phone_number + "')";
                stmt.executeUpdate(sql);
                return "注册成功";
        } catch (ClassNotFoundException | SQLException e) {e.printStackTrace();
        } finally {
            try {if (stmt != null) {conn.close();}
            } catch (SQLException se2) {}
            try {if (conn != null) {conn.close();}
            } catch (SQLException se) {se.printStackTrace();}
        }
        return "注册失败";
    }
}