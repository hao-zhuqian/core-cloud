package com.example.qianduan.changeclass;

import java.util.Date;

/**
 * @Author：Enry
 * @Date:2022/02/16/15:32
 * @Description:
 */
public class BorrowInformation {

    private String applyforname;
    private String applyfornumber;
    private String applyforbook;
    private int applyfortime;
    private Date time;

    public String getApplyforname() {
        return applyforname;
    }

    public void setApplyforname(String applyforname) {
        this.applyforname = applyforname;
    }

    public String getApplyfornumber() {
        return applyfornumber;
    }

    public void setApplyfornumber(String applyfornumber) {
        this.applyfornumber = applyfornumber;
    }

    public String getApplyforbook() {
        return applyforbook;
    }

    public void setApplyforbook(String applyforbook) {
        this.applyforbook = applyforbook;
    }

    public int getApplyfortime() {
        return applyfortime;
    }

    public void setApplyfortime(int applyfortime) {
        this.applyfortime = applyfortime;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "BorrowInformation{" +
                "applyforname='" + applyforname + '\'' +
                ", applyfornumber='" + applyfornumber + '\'' +
                ", applyforbook='" + applyforbook + '\'' +
                ", applyfortime=" + applyfortime +
                ", time=" + time +
                '}';
    }
}
