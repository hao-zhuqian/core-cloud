package com.example.qianduan.servlet;

import com.example.qianduan.changeclass.BorrowInformation;
import com.example.qianduan.jsonchangeclass.JsonChange3;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.text.SimpleDateFormat;

/**
 * @Author：张恩瑞
 * @Date:2022/02/16/15:19
 * @Description:借书功能
 */
@WebServlet("/borrowbook")
public class BorrowServlet extends HttpServlet {

    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/demo?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS ="123456";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /**
         * 操作前端json数据
         */

        // 响应参数格式设置
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out=response.getWriter();
        //使用InputStreamReader对象，获取前端传来的数据.其中
        // request.getInputStream()是读取前端传递来的数据字节流，
        // StandardCharsets.UTF_8是将前端传来的数据转化为UTF-8的编码方式
        InputStreamReader insr = new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8);
        StringBuilder body = new StringBuilder();
        int respInt = insr.read();
        while(respInt!=-1)
        { // 读取请求数据
            //将读取的字节流中的每一个字节转化为字符，然后添加到StringBuilder类型的对象中
            body.append((char) respInt);
            respInt = insr.read();
        }

        JsonChange3 jsonChange3 = new JsonChange3();
        //将StringBuilder类型的对象的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        BorrowInformation borrowInformation = jsonChange3.JsonChangeJavaObject(body.toString());

        //处理时间
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //提交借阅审批时间
        String submit_borrowtime = formatter.format(borrowInformation.getTime());

        //用户名，借阅书籍编号，借阅书籍名称，借阅时长
        String user_name = borrowInformation.getApplyforname();
        int borrowbook_id = Integer.valueOf(borrowInformation.getApplyfornumber()).intValue();
        String borrowbook_name = borrowInformation.getApplyforbook();
        int borrow_time = borrowInformation.getApplyfortime();
        System.out.println(borrowbook_name);

        //审批状态，审批类型
        String control_state = "未审批";
        String control_type = "借书申请";


        /**
         * 操作数据库
         */

        //定义变量
        Connection conn = null;
        Statement stmt = null;

        try{
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            System.out.println("连接数据库...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            //创建stmt对象
            System.out.println(" 实例化Statement对象...");
            stmt = conn.createStatement();
            //执行sql语句
            String sql1,sql2,sql3;
            sql1= "SELECT * FROM book WHERE Book_id='"+borrowbook_id+"'";
            ResultSet rs = stmt.executeQuery(sql1);
            if (!rs.next()) {
                out.print("失败");
            }
            sql3= "SELECT * FROM book WHERE Book_id='"+borrowbook_id+"'";
            ResultSet rs2 = stmt.executeQuery(sql3);
                while (rs2.next()) {
                    String name = rs2.getString("Book_name");
                    if (name.equals(borrowbook_name)) {
                        sql2 = "INSERT borrowrecord(user_name,borrow_time,when_begin,borrowbook_id,return_time,return_book,borrow_state,control_type,control_state,submit_borrowtime,control_borrowtime,whether_agreeborrow,submit_returntime,control_returntime,whether_agreereturn,submit_extendtime,control_extendtime,whether_agreeextend,borrowbook_name,extend_time)" +
                                "VALUES ('" + user_name + "','" + borrow_time + "','','" + borrowbook_id + "','','','待审批','" + control_type + "','" + control_state + "','" + submit_borrowtime + "','','','','','','','','','" + borrowbook_name + "',0)";
                        sql3 = "UPDATE book SET Borrow_State = '已借阅' WHERE Book_name = '"+name+"'";
                        stmt.executeUpdate(sql2);
                        stmt.executeUpdate(sql3);
                        System.out.println(name);
                    } else {
                        out.print("失败");
                    }
                }
            // 完成后关闭
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //释放资源
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
