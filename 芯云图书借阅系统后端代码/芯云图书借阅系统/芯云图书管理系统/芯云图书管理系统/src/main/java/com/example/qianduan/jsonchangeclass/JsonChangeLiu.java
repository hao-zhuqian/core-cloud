package com.example.qianduan.jsonchangeclass;

import com.alibaba.fastjson.JSON;
import com.example.qianduan.changeclass.RegisterClass;

/**
 * @Author 刘媛媛
 * @create 2022/2/18 20:35
 */
public class JsonChangeLiu {
        public RegisterClass JsonChangeJavaObject(String str) {
            RegisterClass registerClass = JSON.parseObject(str, RegisterClass.class);
            return registerClass;
        }

}
