package com.example.qianduan.jsonchangeclass;

import com.alibaba.fastjson.JSON;
import com.example.qianduan.changeclass.ReturnInformation;

/**
 * @Author：Enry
 * @Date:2022/02/17/12:03
 * @Description:
 */
public class JsonChange {
    //将json数据转换为java对象
    public ReturnInformation JsonChangeJavaObject(String str){
        //调用Json.parseObject()方法，第一个参数为json格式的字符串，第二个参数为生成对象的类
        ReturnInformation returnInformation = JSON.parseObject(str,ReturnInformation.class);
        return returnInformation;
    }
}
