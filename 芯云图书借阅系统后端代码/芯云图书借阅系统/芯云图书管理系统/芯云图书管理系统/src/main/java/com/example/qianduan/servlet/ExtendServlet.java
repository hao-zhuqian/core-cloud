package com.example.qianduan.servlet;

import com.example.qianduan.changeclass.ExtendInformation;
import com.example.qianduan.jsonchangeclass.JsonChange2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.text.SimpleDateFormat;

/**
 * @Author：张恩瑞
 * @Date:2022/02/17/17:03
 * @Description:续期功能
 */
@WebServlet("/extendtime")
public class ExtendServlet extends HttpServlet {

    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/demo?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "123456";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /**
         * 操作前端json数据
         */

        // 响应参数格式设置
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out=response.getWriter();
        //使用InputStreamReader对象，获取前端传来的数据.其中
        // request.getInputStream()是读取前端传递来的数据字节流，
        // StandardCharsets.UTF_8是将前端传来的数据转化为UTF-8的编码方式
        InputStreamReader insr = new InputStreamReader(request.getInputStream(), StandardCharsets.UTF_8);
        StringBuilder body = new StringBuilder();
        int respInt = insr.read();
        while(respInt!=-1)
        { // 读取请求数据
            //将读取的字节流中的每一个字节转化为字符，然后添加到StringBuilder类型的对象中
            body.append((char) respInt);
            respInt = insr.read();
        }

        JsonChange2 jsonChange2 = new JsonChange2();
        //将StringBuilder类型的对象的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        ExtendInformation extendInformation = jsonChange2.JsonChangeJavaObject(body.toString());

        //处理时间
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //提交续期审批时间
        String submit_extendtime = formatter.format(extendInformation.getTimess());

        //用户名，书籍编号，书籍名称，续期时长
        String user_name = extendInformation.getExtendname();
        String borrowbook_id = extendInformation.getExtendnumber();
        String borrowbook_name = extendInformation.getExtendbook();
        int extend_time = extendInformation.getExtendtime();


        /**
         * 操作数据库
         */

        //定义变量
        Connection conn = null;
        Statement stmt = null;

        try{
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            System.out.println("连接数据库...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            //创建stmt对象
            System.out.println(" 实例化Statement对象...");
            stmt = conn.createStatement();
            //执行sql语句
            String sql2;
            sql2 = "UPDATE borrowrecord SET extend_time = '"+extend_time+"',submit_extendtime = '"+submit_extendtime+"',control_type='续期申请',control_state='未审批' WHERE user_name = '"+user_name+"' AND borrowbook_id = '"+borrowbook_id+"' AND borrow_state = '正在借阅'";
            stmt.executeUpdate(sql2);
            //out.print("成功");
            // 完成后关闭
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //释放资源
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }






    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
