package com.example.qianduan.method;

import com.example.qianduan.changeclass.message;

import java.sql.*;

/**
 * 查询数据库中的用户表
 * @aurhor：刘媛媛
 */
public class OpenDatabase {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/demo?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "123456";
    public message searchdatabase(String searchway, String usermessage) {
        Connection conn = null;
        Statement stmt = null;
        message message = null;
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");
            stmt = conn.createStatement();
            String sql;
            ResultSet rs;
            if ("user_name".equals(searchway)) {
                sql = "SELECT * FROM user WHERE user_name='" + usermessage + "'";
                rs = stmt.executeQuery(sql);
            } else if ("phone_number".equals(searchway)) {
                sql = "SELECT * FROM user WHERE phone_number='" + usermessage + "'";
                rs = stmt.executeQuery(sql);
            } else if ("email".equals(searchway)) {
                sql = "SELECT * FROM user WHERE email='" + usermessage + "'";
                rs=stmt.executeQuery(sql);
                    if (!rs.next()) {
                        return new message("该邮箱未注册！", "无", "无", "0", "无", "无", "无", "无", "无", "0", "无", "无", new Date(0000 - 00 - 00), 0, "无", "无","0");
                    }else {
                        do{
                            String user_name = rs.getString("user_name");
                            String password = rs.getString("password");
                            String phone_number = rs.getString("phone_number");
                            String QQ_number = rs.getString("QQ_number");
                            String email = rs.getString("email");
                            String name = rs.getString("name");
                            String sex = rs.getString("sex");
                            String nper = rs.getString("nper");
                            String direction = rs.getString("direction");
                            String student_id = rs.getString("student_id");
                            String professional_class = rs.getString("professional_class");
                            String campus = rs.getString("campus");
                            Date registration_date = rs.getDate("registration_date");
                            int overdue_number = rs.getInt("overdue_number");
                            String administrator = rs.getString("administrator");
                            String borrow_status = rs.getString("borrow_status");
                            String picture = rs.getString("picture");
                            message = new message(user_name, password, phone_number, QQ_number, email, name, sex, nper, direction, student_id, professional_class, campus, registration_date, overdue_number, administrator, borrow_status, picture);
                            System.out.println(message);
                        }
                        while (rs.next());
                    }
            } else if ("QQ_number".equals(searchway)) {
                sql = "SELECT * FROM user WHERE QQ_number='" + usermessage + "'";
                rs = stmt.executeQuery(sql);
            }else if ("delete".equals(searchway)){
                sql = "DELETE FROM user WHERE user_name='" + usermessage + "'";
                stmt.executeUpdate(sql);
                return new message("注销成功", "无", "无", "0", "无", "无", "无", "无", "无", "0", "无", "无", new Date(0000 - 00 - 00), 0, "无", "无","0");
            } else {
                return new message("searchway错误", "无", "无", "0", "无", "无", "无", "无", "无", "0", "无", "无", new Date(0000 - 00 - 00), 0, "无", "无","0");
            }
                while (rs.next()) {
                    String user_name = rs.getString("user_name");
                    String password = rs.getString("password");
                    String phone_number = rs.getString("phone_number");
                    String QQ_number = rs.getString("QQ_number");
                    String email = rs.getString("email");
                    String name = rs.getString("name");
                    String sex = rs.getString("sex");
                    String nper = rs.getString("nper");
                    String direction = rs.getString("direction");
                    String student_id = rs.getString("student_id");
                    String professional_class = rs.getString("professional_class");
                    String campus = rs.getString("campus");
                    Date registration_date = rs.getDate("registration_date");
                    int overdue_number = rs.getInt("overdue_number");
                    String administrator = rs.getString("administrator");
                    String borrow_status = rs.getString("borrow_status");
                    String picture = rs.getString("picture");
                    message= new message(user_name, password, phone_number, QQ_number, email, name, sex, nper, direction, student_id, professional_class, campus, registration_date, overdue_number, administrator, borrow_status,picture);
                System.out.println(message);
            }
            rs.close();
        } catch (ClassNotFoundException | SQLException e) {e.printStackTrace();
        } finally {
            try {if (stmt != null) {conn.close();}
            } catch (SQLException se2) {}
            try {if (conn != null) {conn.close();}
            } catch (SQLException se) {se.printStackTrace();}
        }
        if (message != null) {
            return message;
        } else {
            return new message("登录信息不存在", "无", "无", "0", "无", "无", "无", "无", "无", "0", "无", "无", new Date(0000 - 00 - 00), 0, "无", "无","");
        }
    }
}