package com.example.qianduan.changeclass;

import java.sql.Date;

public class ReturnSearch {
    private String user_name;
    private String password;
    private String phone_number;
    private String QQ_number;
    private String email;
    private String name;
    private String sex;
    private String nper;
    private String direction;
    private String student_id;
    private String professional_class;
    private String campus;
    private Date registration_date;
    private int overdue_number;
    private String administrator;
    private String borrow_status;

    public ReturnSearch(String user_name, String password, String phone_number, String QQ_number, String email, String name, String sex, String nper, String direction, String student_id, String professional_class, String campus, Date registration_date, int overdue_number, String administrator,  String borrow_status) {
        this.user_name = user_name;
        this.password = password;
        this.phone_number = phone_number;
        this.QQ_number = QQ_number;
        this.email = email;
        this.name = name;
        this.sex = sex;
        this.nper = nper;
        this.direction = direction;
        this.student_id = student_id;
        this.professional_class = professional_class;
        this.campus = campus;
        this.registration_date = registration_date;
        this.overdue_number = overdue_number;
        this.administrator = administrator;
        this.borrow_status = borrow_status;
    }
    public ReturnSearch(){

    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getQQ_number() {
        return QQ_number;
    }

    public void setQQ_number(String QQ_number) {
        this.QQ_number = QQ_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNper() {
        return nper;
    }

    public void setNper(String nper) {
        this.nper = nper;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getProfessional_class() {
        return professional_class;
    }

    public void setProfessional_class(String professional_class) {
        this.professional_class = professional_class;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public Date getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(Date registration_date) {
        this.registration_date = registration_date;
    }

    public int getOverdue_number() {
        return overdue_number;
    }

    public void setOverdue_number(int overdue_number) {
        this.overdue_number = overdue_number;
    }

    public String getAdministrator() {
        return administrator;
    }

    public void setAdministrator(String administrator) {
        this.administrator = administrator;
    }

    public String getBorrow_status() {
        return borrow_status;
    }

    public void setBorrow_status(String borrow_status) {
        this.borrow_status = borrow_status;
    }

    @Override
    public String toString() {
        return  "user_name='" + user_name + '\'' +
                ", password='" + password + '\'' +
                ", phone_number='" + phone_number + '\'' +
                ", QQ_number=" + QQ_number +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", nper='" + nper + '\'' +
                ", direction='" + direction + '\'' +
                ", student_id='" + student_id + '\'' +
                ", professional_class='" + professional_class + '\'' +
                ", campus='" + campus + '\'' +
                ", registration_date=" + registration_date +
                ", overdue_number=" + overdue_number +
                ", administrator='" + administrator + '\'' +
                ", borrow_status='" + borrow_status + '\'' ;
    }
}
