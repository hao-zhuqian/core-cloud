package com.example.qianduan.changeclass;

public class ReturnUserbook {
    private int Book_id;
    private String Book_name;
    private String Book_type;
    private String Author;
    private String Publisher;
    private String Borrow_state;

    public ReturnUserbook() {
    }

    public ReturnUserbook(int book_id, String book_name, String book_type, String author, String publisher, String borrow_state) {
        Book_id = book_id;
        Book_name = book_name;
        Book_type = book_type;
        Author = author;
        Publisher = publisher;
        Borrow_state = borrow_state;
    }

    public int getBook_id() {
        return Book_id;
    }

    public void setBook_id(int book_id) {
        Book_id = book_id;
    }

    public String getBook_name() {
        return Book_name;
    }

    public void setBook_name(String book_name) {
        Book_name = book_name;
    }

    public String getBook_type() {
        return Book_type;
    }

    public void setBook_type(String book_type) {
        Book_type = book_type;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public String getPublisher() {
        return Publisher;
    }

    public void setPublisher(String publisher) {
        Publisher = publisher;
    }

    public String getBorrow_state() {
        return Borrow_state;
    }

    public void setBorrow_state(String borrow_state) {
        Borrow_state = borrow_state;
    }

    @Override
    public String toString() {
        return "Book_id=" + Book_id +
                ", Book_name='" + Book_name + '\'' +
                ", Book_type='" + Book_type + '\'' +
                ", Author='" + Author + '\'' +
                ", Publisher='" + Publisher + '\'' +
                ", Borrow_state='" + Borrow_state + '\'' ;
    }
}
