package com.example.qianduan.servlet;
import com.alibaba.fastjson.JSONObject;
import com.example.qianduan.changeclass.Addbook;
import com.example.qianduan.changeclass.Control2Class;
import com.example.qianduan.changeclass.ReturnControl;
import com.example.qianduan.jsonchangeclass.JsonChangeAddbook;
import com.example.qianduan.jsonchangeclass.JsonChangeControl2;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * 管理员同意/拒绝审批功能
 * @author：石烨
 */
public class Control2 extends HttpServlet {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/demo?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS ="123456";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection conn = null;
        Statement stmt = null;
        int time=0;
        // 响应参数格式设置
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        //将此字符串对象输出
        PrintWriter out=resp.getWriter();
        //使用InputStreamReader对象，获取前端传来的数据.其中
        // request.getInputStream()是读取前端传递来的数据字节流，
        // StandardCharsets.UTF_8是将前端传来的数据转化为UTF-8的编码方式
        InputStreamReader insr = new InputStreamReader(req.getInputStream(), StandardCharsets.UTF_8);
        StringBuilder body = new StringBuilder();
        int respInt = insr.read();
        while(respInt!=-1) { // 读取请求数据
            //将读取的字节流中的每一个字节转化为字符，然后添加到StringBuilder类型的对象中
            body.append((char) respInt);
            respInt = insr.read();
        }
        //out的print方法可以输出对象
        // out.print(body);
        JsonChangeControl2 jsonChangeControl2 = new JsonChangeControl2();
        //将StringBuilder类型的对象的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        Control2Class control2Class = jsonChangeControl2.JsonChangeJavaObject(body.toString());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(control2Class.getAuditTime());
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            System.out.println("连接数据库...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 查询用户信息
            System.out.println("正在查询...");
            stmt = conn.createStatement();
            if (control2Class.getInitiateType().equals("借书申请")){
                if (control2Class.getPass().equals("false")){
//                    String sql1 = "UPDATE borrowrecord SET control_borrowtime='"+dateString+"' WHERE id='"+control2Class.getNumber()+"'";
//                    String sql2 = "UPDATE borrowrecord SET whether_agreeborrow='拒绝' WHERE id='"+control2Class.getNumber()+"'";
//                    String sql3 = "UPDATE borrowrecord SET control_state='已审批' WHERE id='"+control2Class.getNumber()+"'";
//                    String sql4 = "UPDATE borrowrecord SET borrow_state='借书申请被拒绝' WHERE id='"+control2Class.getNumber()+"'";
//                    sql5 = "UPDATE book SET Borrow_State = '未借阅' WHERE  = '"+Book_id+"'";
//                    stmt.executeUpdate(sql1);
//                    stmt.executeUpdate(sql2);
//                    stmt.executeUpdate(sql3);
//                    stmt.executeUpdate(sql4);
//                    stmt.executeUpdate(sql5);
                    String sql6 = "SELECT * FROM borrowrecord WHERE id='"+control2Class.getNumber()+"' ";
                    ResultSet rs= stmt.executeQuery(sql6);
                    while (rs.next()){
                        String ID = rs.getString("borrowbook_id");
                        String sql1 = "UPDATE borrowrecord SET control_borrowtime='"+dateString+"' WHERE id='"+control2Class.getNumber()+"'";
                    String sql2 = "UPDATE borrowrecord SET whether_agreeborrow='拒绝' WHERE id='"+control2Class.getNumber()+"'";
                    String sql3 = "UPDATE borrowrecord SET control_state='已审批' WHERE id='"+control2Class.getNumber()+"'";
                    String sql4 = "UPDATE borrowrecord SET borrow_state='借书申请被拒绝' WHERE id='"+control2Class.getNumber()+"'";
                    String sql5 = "UPDATE book SET Borrow_State='未借阅'WHERE Book_id ='"+ID+"'";
                    stmt.executeUpdate(sql1);
                    stmt.executeUpdate(sql2);
                    stmt.executeUpdate(sql3);
                    stmt.executeUpdate(sql4);
                    stmt.executeUpdate(sql5);
                    }
                }
                if (control2Class.getPass().equals("true")){
                    String sql1 = "UPDATE borrowrecord SET control_borrowtime='"+dateString+"' WHERE id='"+control2Class.getNumber()+"'";
                    String sql2 = "UPDATE borrowrecord SET whether_agreeborrow='同意' WHERE id='"+control2Class.getNumber()+"'";
                    String sql3 = "UPDATE borrowrecord SET borrow_state='正在借阅' WHERE id='"+control2Class.getNumber()+"'";
                    String sql4 = "UPDATE borrowrecord SET control_state='已审批' WHERE id='"+control2Class.getNumber()+"'";
                    String sql5 = "UPDATE user SET borrow_status='已借阅' WHERE user_name='"+control2Class.getNumber()+"'";
                    stmt.executeUpdate(sql1);
                    stmt.executeUpdate(sql2);
                    stmt.executeUpdate(sql3);
                    stmt.executeUpdate(sql4);
                    stmt.executeUpdate(sql5);
                    String sql6 = "SELECT * FROM borrowrecord WHERE id='"+control2Class.getNumber()+"'";
                    ResultSet rs = stmt.executeQuery(sql6);
                    while (rs.next()){
                        String borrowbook_name = rs.getString("borrowbook_name");
                        String sql7 = "UPDATE book SET Borrow_State='已借阅' WHERE Book_name='"+borrowbook_name+"'";
                        stmt.executeUpdate(sql7);
                    }
                }
            }
            if (control2Class.getInitiateType().equals("还书申请")) {
                if (control2Class.getPass().equals("false")) {
                    String sql1 = "UPDATE borrowrecord SET control_returntime='" + dateString + "' WHERE id='" + control2Class.getNumber() + "'";
                    String sql2 = "UPDATE borrowrecord SET whether_agreereturn='拒绝' WHERE id='" + control2Class.getNumber() + "'";
                    String sql3 = "UPDATE borrowrecord SET control_state='已审批' WHERE id='" + control2Class.getNumber() + "'";
                    stmt.executeUpdate(sql1);
                    stmt.executeUpdate(sql2);
                    stmt.executeUpdate(sql3);
                }
                if (control2Class.getPass().equals("true")) {
                    String sql1 = "UPDATE borrowrecord SET control_returntime='" + dateString + "' WHERE id='" + control2Class.getNumber() + "'";
                    String sql2 = "UPDATE borrowrecord SET whether_agreereturn='同意' WHERE id='" + control2Class.getNumber() + "'";
                    String sql6 = "UPDATE borrowrecord SET control_state='已审批' WHERE id='" + control2Class.getNumber() + "'";
                    String sql3 = "SELECT * FROM borrowrecord WHERE id='" + control2Class.getNumber() + "'";
                    stmt.executeUpdate(sql1);
                    stmt.executeUpdate(sql2);
                    stmt.executeUpdate(sql6);
                    ResultSet rs = stmt.executeQuery(sql3);
                    while (rs.next()) {
                        int borrow_time = rs.getInt("borrow_time");
                        String control_borrowtime = rs.getString("control_borrowtime");
                        String submit_returntime = rs.getString("submit_returntime");
                      //  SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        long curSysTime1 =0;
                        try {
                            //同意借书审批的时间
                            curSysTime1 = formatter.parse(control_borrowtime).getTime();
                            //本来需要还书的时间
                            long temp=(long)borrow_time;
                            long deadtime=curSysTime1+temp*24*60*60*1000;
                            //提交还书审批的时间
                            long   curSysTime2 = 0;
                            curSysTime2 = formatter.parse(submit_returntime).getTime();
                            if (curSysTime2>deadtime){
                                String sql4 = "SELECT * FROM user WHERE user_name='" + control2Class.getId() + "'";
                                ResultSet rs2 = stmt.executeQuery(sql4);
                                while (rs2.next()){
                                    int overdue_number = rs2.getInt("overdue_number");
                                    int overdue_number2=overdue_number+1;
                                    String sql5 = "UPDATE user SET overdue_number='"+overdue_number2+"'  WHERE user_name='"+control2Class.getId()+"'";
                                    stmt.executeUpdate(sql5);
                                }
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (control2Class.getInitiateType().equals("续期申请")){
                if (control2Class.getPass().equals("false")){
                    String sql1 = "UPDATE borrowrecord SET control_extendtime='"+dateString+"' WHERE id='"+control2Class.getNumber()+"'";
                    String sql2 = "UPDATE borrowrecord SET whether_agreeextend='拒绝' WHERE id='"+control2Class.getNumber()+"'";
                    String sql3 = "UPDATE borrowrecord SET control_state='已审批' WHERE id='"+control2Class.getNumber()+"'";
                    stmt.executeUpdate(sql1);
                    stmt.executeUpdate(sql2);
                    stmt.executeUpdate(sql3);
                }
                if (control2Class.getPass().equals("true")){
                    String sql1 = "UPDATE borrowrecord SET control_extendtime='"+dateString+"' WHERE id='"+control2Class.getNumber()+"'";
                    String sql2 = "UPDATE borrowrecord SET whether_agreeextend='同意' WHERE id='"+control2Class.getNumber()+"'";
                    String sql3 = "UPDATE borrowrecord SET control_state='已审批' WHERE id='"+control2Class.getNumber()+"'";
                    stmt.executeUpdate(sql1);
                    stmt.executeUpdate(sql2);
                    stmt.executeUpdate(sql3);
                    String sql4 = "SELECT * FROM borrowrecord WHERE id='"+control2Class.getNumber()+"'";
                    ResultSet rs = stmt.executeQuery(sql4);
                    while (rs.next()) {
                        int borrow_time = rs.getInt("borrow_time");
                        int extend_time = rs.getInt("extend_time");
                        time=borrow_time+extend_time;
                        String sql5 = "UPDATE borrowrecord SET borrow_time='"+time+"' WHERE id='"+control2Class.getNumber()+"'";
                        stmt.executeUpdate(sql5);
                    }
                }
            }
          //  out.write("成功！");
            //录入完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
