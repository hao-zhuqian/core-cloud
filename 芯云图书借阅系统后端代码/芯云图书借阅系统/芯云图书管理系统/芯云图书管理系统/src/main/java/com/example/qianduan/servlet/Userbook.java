package com.example.qianduan.servlet;

import com.alibaba.fastjson.JSONObject;
import com.example.qianduan.changeclass.ReturnSearch;
import com.example.qianduan.changeclass.ReturnUserbook;
import com.example.qianduan.changeclass.Search;
import com.example.qianduan.changeclass.UserbookClass;
import com.example.qianduan.jsonchangeclass.JsonChangeSearch;
import com.example.qianduan.jsonchangeclass.JsonChangeUserbook;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户查阅书籍功能
 * @author：曹睿
 */
public class Userbook extends HttpServlet {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/demo?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "123456";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection conn = null;
        Statement stmt = null;
        // 响应参数格式设置
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        //将此字符串对象输出
        PrintWriter out=resp.getWriter();
        //使用InputStreamReader对象，获取前端传来的数据.其中
        // request.getInputStream()是读取前端传递来的数据字节流，
        // StandardCharsets.UTF_8是将前端传来的数据转化为UTF-8的编码方式
        InputStreamReader insr = new InputStreamReader(req.getInputStream(), StandardCharsets.UTF_8);
        StringBuilder body = new StringBuilder();
        int respInt = insr.read();
        while(respInt!=-1) { // 读取请求数据
            //将读取的字节流中的每一个字节转化为字符，然后添加到StringBuilder类型的对象中
            body.append((char) respInt);
            respInt = insr.read();
        }
        JsonChangeUserbook jsonChangeUserbook = new JsonChangeUserbook();
        //将StringBuilder类型的对象的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        UserbookClass userbookClass = jsonChangeUserbook.JsonChangeJavaObject(body.toString());
        List<ReturnUserbook> list=new ArrayList<ReturnUserbook>();
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            System.out.println("连接数据库...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 查询用户信息
            System.out.println("正在查询...");
            stmt = conn.createStatement();
            if (userbookClass.getFindWays().equals("Book_name")){
                String sql = "SELECT * FROM book WHERE Book_name='"+userbookClass.getInput()+"'";
                ResultSet rs = stmt.executeQuery(sql);
                while(rs.next()){
                    int Book_id = rs.getInt("Book_id");
                    String Book_name = rs.getString("Book_name");
                    String Book_type = rs.getString("Book_type");
                    String Author = rs.getString("Author");
                    String Publisher = rs.getString("Publisher");
                    String Borrow_State = rs.getString("Borrow_State");
                    list.add(new ReturnUserbook(Book_id,Book_name,Book_type,Author,Publisher,Borrow_State));
                }

            }
            if (userbookClass.getFindWays().equals("Book_type")){
                String sql = "SELECT * FROM book WHERE Book_type='"+userbookClass.getInput()+"'";
                ResultSet rs = stmt.executeQuery(sql);
                while(rs.next()){
                    int Book_id = rs.getInt("Book_id");
                    String Book_name = rs.getString("Book_name");
                    String Book_type = rs.getString("Book_type");
                    String Author = rs.getString("Author");
                    String Publisher = rs.getString("Publisher");
                    String Borrow_State = rs.getString("Borrow_State");
                    list.add(new ReturnUserbook(Book_id,Book_name,Book_type,Author,Publisher,Borrow_State));
                }

            }
            if (userbookClass.getFindWays().equals("Borrow_State")){
                String sql = "SELECT * FROM book WHERE Borrow_State='"+userbookClass.getInput()+"'";
                ResultSet rs = stmt.executeQuery(sql);
                while(rs.next()){
                    int Book_id = rs.getInt("Book_id");
                    String Book_name = rs.getString("Book_name");
                    String Book_type = rs.getString("Book_type");
                    String Author = rs.getString("Author");
                    String Publisher = rs.getString("Publisher");
                    String Borrow_State = rs.getString("Borrow_State");
                    list.add(new ReturnUserbook(Book_id,Book_name,Book_type,Author,Publisher,Borrow_State));
                }
            }
            String jsonObject= JSONObject.toJSONString(list);
            out.println(jsonObject);
            if (list.size()==0){
                out.print("失败");
            }
            //录入完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
