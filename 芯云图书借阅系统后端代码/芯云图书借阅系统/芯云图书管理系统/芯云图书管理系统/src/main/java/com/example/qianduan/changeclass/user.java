package com.example.qianduan.changeclass;

/**
 * @Author 刘媛媛
 * @create 2022/2/16 13:19
 */
public class user {
    private String user_message;
    private String password;
    private String searchway;

    public String getUser_message() {
        return user_message;
    }

    public void setUser_message(String user_message) {
        this.user_message = user_message;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSearchway() {
        return searchway;
    }

    public void setSearchway(String searchway) {
        this.searchway = searchway;
    }

    @Override
    public String toString() {
        return "user{" +
                "user_message='" + user_message + '\'' +
                ", password='" + password + '\'' +
                ", searchway='" + searchway + '\'' +
                '}';
    }
}
