package com.example.qianduan.servlet;

import com.example.qianduan.changeclass.Addbook;
import com.example.qianduan.changeclass.Reducebook;
import com.example.qianduan.jsonchangeclass.JsonChangeAddbook;
import com.example.qianduan.jsonchangeclass.JsonChangeReducebook;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.*;

/**
 * 管理员删除书籍功能
 * @author：石烨
 */
public class ReduceMangerBook extends HttpServlet {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/demo?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "123456";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection conn = null;
        Statement stmt = null;
        // 响应参数格式设置
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        //将此字符串对象输出
        PrintWriter out=resp.getWriter();
        //使用InputStreamReader对象，获取前端传来的数据.其中
        // request.getInputStream()是读取前端传递来的数据字节流，
        // StandardCharsets.UTF_8是将前端传来的数据转化为UTF-8的编码方式
        InputStreamReader insr = new InputStreamReader(req.getInputStream(), StandardCharsets.UTF_8);
        StringBuilder body = new StringBuilder();
        int respInt = insr.read();
        while(respInt!=-1) { // 读取请求数据
            //将读取的字节流中的每一个字节转化为字符，然后添加到StringBuilder类型的对象中
            body.append((char) respInt);
            respInt = insr.read();
        }
        JsonChangeReducebook jsonChangeReducebook = new JsonChangeReducebook();
        //将StringBuilder类型的对象的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        Reducebook reducebook = jsonChangeReducebook.JsonChangeJavaObject(body.toString());
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            System.out.println("连接数据库...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 查询用户信息
            System.out.println("正在查询...");
            stmt = conn.createStatement();
            String sql = "SELECT * FROM book WHERE Book_name='"+reducebook.getBooknames()+"' AND Book_id='"+reducebook.getId()+"'";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String Book_name=rs.getString("Book_name");
                if (reducebook.getBooknames().equals(Book_name)) {
                    sql = "DELETE FROM book WHERE Book_name='"+reducebook.getBooknames()+"'";
                    stmt.executeUpdate(sql);
                  //  out.println("成功");
                    System.out.println("删除成功");
                }
            }
            //录入完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
