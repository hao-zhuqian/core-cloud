package com.example.qianduan.method;

import java.sql.*;

/**
 * 修改数据库用户表中内容
 * @author：刘媛媛
 */
public class ChangeDatabase {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/demo?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "123456";
    public String writedatabase(String useclass,String usemessage,String changeclass,String changemessage) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");
            stmt = conn.createStatement();
            if("user_name".equals(changeclass)){
            String sql = "SELECT * FROM user WHERE user_name='" + changemessage + "'";
            ResultSet rs = stmt.executeQuery(sql);
                while (rs.next()){
            if (rs.getString("user_name").equals(changemessage)) {
                    return "该用户名已存在";}}}
            if("phone_number".equals(changeclass)){
            String sql = "SELECT * FROM user WHERE phone_number='" + changemessage + "'";
                ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                if (rs.getString("phone_number").equals(changemessage)) {
                    return "该电话号码已注册";}}}
            String sql = "SELECT * FROM user WHERE email='" + changemessage + "'";
            ResultSet  rs = stmt.executeQuery(sql);
            while (rs.next()) {
                if (rs.getString("email").equals(changemessage)) {
                    return "该邮箱已注册";}}
             sql = "UPDATE user SET "+changeclass+"='" + changemessage + "'WHERE "+useclass+"='" + usemessage + "';";
            stmt.executeUpdate(sql);
            return "修改成功";
        } catch (ClassNotFoundException | SQLException e) {e.printStackTrace();}
        finally {try {
                if (stmt != null) {conn.close();}
            } catch (SQLException se2) {}
            try {if (conn != null) {conn.close();}
            } catch (SQLException se) {se.printStackTrace();}
        }
        return "修改失败";
    }
}