package com.example.qianduan.changeclass;

/**
 * findfield:查询方式
 * value:改查询方式下的值
 * @author 石烨
 */
public class Search {
    private String FindWay;
    private String inputcontent;

    public Search(){

    }

    public String getFindWay() {
        return FindWay;
    }

    public void setFindWay(String findWay) {
        FindWay = findWay;
    }

    public String getInputcontent() {
        return inputcontent;
    }

    public void setInputcontent(String inputcontent) {
        this.inputcontent = inputcontent;
    }
}
