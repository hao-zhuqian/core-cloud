package com.example.qianduan.jsonchangeclass;

import com.alibaba.fastjson.JSON;
import com.example.qianduan.changeclass.Search;
import com.example.qianduan.changeclass.UserbookClass;

public class JsonChangeUserbook {
    public UserbookClass JsonChangeJavaObject(String str) {
        UserbookClass userbookClass = JSON.parseObject(str, UserbookClass.class);
        return userbookClass;
    }
}
