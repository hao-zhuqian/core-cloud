package com.example.qianduan.jsonchangeclass;

import com.alibaba.fastjson.JSON;
import com.example.qianduan.changeclass.BorrowInformation;

/**
 * @Author：Enry
 * @Date:2022/02/13/14:20
 * @Description:
 */

public class JsonChange3 {

    //将json数据转换为java对象
    public BorrowInformation JsonChangeJavaObject(String str){
        //调用Json.parseObject()方法，第一个参数为json格式的字符串，第二个参数为生成对象的类
        BorrowInformation borrowInformation = JSON.parseObject(str,BorrowInformation.class);
        return borrowInformation;
    }
}