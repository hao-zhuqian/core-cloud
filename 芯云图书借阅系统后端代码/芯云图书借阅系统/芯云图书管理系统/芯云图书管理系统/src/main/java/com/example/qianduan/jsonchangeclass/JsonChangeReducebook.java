package com.example.qianduan.jsonchangeclass;

import com.alibaba.fastjson.JSON;
import com.example.qianduan.changeclass.Reducebook;

public class JsonChangeReducebook {
    public Reducebook JsonChangeJavaObject(String str) {
        Reducebook reducebook = JSON.parseObject(str, Reducebook.class);
        return reducebook;
    }
}
