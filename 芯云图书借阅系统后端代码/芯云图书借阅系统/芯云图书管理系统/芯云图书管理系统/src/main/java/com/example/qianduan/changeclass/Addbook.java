package com.example.qianduan.changeclass;

public class Addbook {
    private String bookname;
    private String bookform;
    private String bookauthor;
    private String bookpress;

    public Addbook(){

    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getBookform() {
        return bookform;
    }

    public void setBookform(String bookform) {
        this.bookform = bookform;
    }

    public String getBookauthor() {
        return bookauthor;
    }

    public void setBookauthor(String bookauthor) {
        this.bookauthor = bookauthor;
    }

    public String getBookpress() {
        return bookpress;
    }

    public void setBookpress(String bookpress) {
        this.bookpress = bookpress;
    }
}
