package com.example.qianduan.jsonchangeclass;

import com.alibaba.fastjson.JSON;
import com.example.qianduan.changeclass.Addbook;
import com.example.qianduan.changeclass.Control2Class;

public class JsonChangeControl2 {
    public Control2Class JsonChangeJavaObject(String str) {
        Control2Class control2Class = JSON.parseObject(str, Control2Class.class);
        return control2Class;
    }
}
