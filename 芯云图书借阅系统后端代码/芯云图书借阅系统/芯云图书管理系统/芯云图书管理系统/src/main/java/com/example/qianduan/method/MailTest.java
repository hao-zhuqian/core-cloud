package com.example.qianduan.method;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @Author 世外桃媛
 * @create 2022/2/17 16:27
 * 发送验证码
 */
public class MailTest {
    public void mail(String usermail, String number) throws MessagingException {
        Properties props = new Properties();
        props.put("mail.smtp.port", "587");
        props.put("mail.user", "1776557437@qq.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.password", "cjgwlczkqefxccif");
        props.put("mail.smtp.host", "smtp.qq.com");
        Authenticator authenticator = new Authenticator() {@Override
        public PasswordAuthentication getPasswordAuthentication() {
        String userName = props.getProperty("mail.user");
        String password = props.getProperty("mail.password");
        return new PasswordAuthentication(userName, password);}};
        Session mailsession = Session.getInstance(props, authenticator);
        MimeMessage message = new MimeMessage(mailsession);
        InternetAddress form = new InternetAddress(props.getProperty("mail.user"));
        message.setFrom(form);
        InternetAddress to = new InternetAddress(usermail);
        message.setRecipient(Message.RecipientType.TO, to);
        message.setSubject("芯云图书借阅系统");
        message.setContent("您正在操作芯云图书借阅系统，\n您的邮箱验证码为 " + number + "\n请尽快完成验证！", "text/html;charset=UTF-8");
        Transport.send(message);
        System.out.println("已发送邮箱验证码");
    }
}