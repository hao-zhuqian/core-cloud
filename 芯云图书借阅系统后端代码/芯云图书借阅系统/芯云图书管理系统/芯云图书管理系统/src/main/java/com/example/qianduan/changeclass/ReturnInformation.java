package com.example.qianduan.changeclass;

import java.util.Date;

/**
 * @Author：Enry
 * @Date:2022/02/17/11:33
 * @Description:
 */
public class ReturnInformation {

    private String returnname;
    private String returnbook;
    private Date times;
    private String returnnumber;

    public String getReturnname() {
        return returnname;
    }

    public void setReturnname(String returnname) {
        this.returnname = returnname;
    }

    public String getReturnbook() {
        return returnbook;
    }

    public void setReturnbook(String returnbook) {
        this.returnbook = returnbook;
    }

    public Date getTimes() {
        return times;
    }

    public void setTimes(Date times) {
        this.times = times;
    }

    public String getReturnnumber() {
        return returnnumber;
    }

    public void setReturnnumber(String returnnumber) {
        this.returnnumber = returnnumber;
    }

    @Override
    public String toString() {
        return "ReturnInformation{" +
                "returnname='" + returnname + '\'' +
                ", returnbook='" + returnbook + '\'' +
                ", times=" + times +
                ", returnnumber='" + returnnumber + '\'' +
                '}';
    }
}
