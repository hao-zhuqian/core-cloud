package com.example.qianduan.jsonchangeclass;

import com.alibaba.fastjson.JSON;
import com.example.qianduan.changeclass.Search;

public class JsonChangeSearch {
    public Search JsonChangeJavaObject(String str) {
        Search search = JSON.parseObject(str, Search.class);
        return search;
    }
}
