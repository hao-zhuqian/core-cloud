package com.example.qianduan.changeclass;

import java.util.Date;

public class Control2Class {
    private int Number;
    private String Id;
    private String Pass;
    private Date AuditTime;
    private String InitiateType;

    public Control2Class() {
    }

    public int getNumber() {
        return Number;
    }

    public void setNumber(int number) {
        Number = number;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String pass) {
        Pass = pass;
    }

    public Date getAuditTime() {
        return AuditTime;
    }

    public void setAuditTime(Date auditTime) {
        AuditTime = auditTime;
    }

    public String getInitiateType() {
        return InitiateType;
    }

    public void setInitiateType(String initiateType) {
        InitiateType = initiateType;
    }
}
