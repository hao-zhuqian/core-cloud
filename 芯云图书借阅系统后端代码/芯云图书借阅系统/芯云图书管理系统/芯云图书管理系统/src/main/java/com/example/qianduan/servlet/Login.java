package com.example.qianduan.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.qianduan.method.OpenDatabase;
import com.example.qianduan.changeclass.user;
import com.example.qianduan.changeclass.message;


import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

/**
 * 登录功能
 * @author：刘媛媛
 */
@WebServlet(name = "Login", value = "/login")
public class Login extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        // 响应参数格式设置
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        InputStreamReader insr = new InputStreamReader(req.getInputStream(), StandardCharsets.UTF_8);
        StringBuilder message = new StringBuilder();
        int respInt = insr.read();
        while (respInt != -1) {
            // 读取请求数据,将读取的字节流中的每一个字节转化为字符，然后添加到StringBuilder类型的对象中
            message.append((char) respInt);
            respInt = insr.read();
        }
        //将StringBuilder类型的对象message的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        user user = JSON.parseObject(message.toString(),user.class);
        System.out.println(user);
        String searchway=String.valueOf(user.getSearchway());
        String loginmessage =String.valueOf(user.getUser_message());
        String password =String.valueOf(user.getPassword());
        OpenDatabase openDatabase=new OpenDatabase();
        message usermessage =openDatabase.searchdatabase(searchway, loginmessage);
        if ("email".equals(searchway)) {
            String jsonObject= JSONObject.toJSONString(usermessage);
            out.println(jsonObject);
        }else if(password.equals(usermessage.getPassword())){
            String jsonObject= JSONObject.toJSONString(usermessage);
            out.println(jsonObject);
        }else if (!password.equals(usermessage.getPassword())){
            String jsonObject= JSONObject.toJSONString("密码错误");
            out.println(jsonObject);
        } else {
            String jsonObject= JSONObject.toJSONString("登录失败");
            out.println(jsonObject);}}
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}