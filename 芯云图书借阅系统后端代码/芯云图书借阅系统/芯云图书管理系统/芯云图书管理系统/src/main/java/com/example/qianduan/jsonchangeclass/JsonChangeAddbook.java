package com.example.qianduan.jsonchangeclass;

import com.alibaba.fastjson.JSON;
import com.example.qianduan.changeclass.Addbook;


public class JsonChangeAddbook {
    public Addbook JsonChangeJavaObject(String str) {
        Addbook addbook = JSON.parseObject(str, Addbook.class);
        return addbook;
    }
}
