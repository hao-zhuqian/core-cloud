package com.example.qianduan.changeclass;

import java.util.Date;

/**
 * @Author：Enry
 * @Date:2022/02/17/17:03
 * @Description:
 */
public class ExtendInformation {

    private String extendname;
    private String extendbook;
    private String extendnumber;
    private  int extendtime;
    private Date timess;

    public String getExtendname() {
        return extendname;
    }

    public void setExtendname(String extendname) {
        this.extendname = extendname;
    }

    public String getExtendbook() {
        return extendbook;
    }

    public void setExtendbook(String extendbook) {
        this.extendbook = extendbook;
    }

    public String getExtendnumber() {
        return extendnumber;
    }

    public void setExtendnumber(String extendnumber) {
        this.extendnumber = extendnumber;
    }

    public int getExtendtime() {
        return extendtime;
    }

    public void setExtendtime(int extendtime) {
        this.extendtime = extendtime;
    }

    public Date getTimess() {
        return timess;
    }

    public void setTimess(Date timess) {
        this.timess = timess;
    }

    @Override
    public String toString() {
        return "ExtendInformation{" +
                "extendname='" + extendname + '\'' +
                ", extendbook='" + extendbook + '\'' +
                ", extendnumber='" + extendnumber + '\'' +
                ", extendtime=" + extendtime +
                ", timess=" + timess +
                '}';
    }
}
