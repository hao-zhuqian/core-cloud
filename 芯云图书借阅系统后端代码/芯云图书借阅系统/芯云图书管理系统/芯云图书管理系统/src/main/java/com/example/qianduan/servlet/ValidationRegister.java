package com.example.qianduan.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.qianduan.changeclass.message;
import com.example.qianduan.method.MailTest;
import com.example.qianduan.changeclass.Enroll;
import com.example.qianduan.method.OpenDatabase;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Random;

/**
 * @Author 刘媛媛
 * @create 2022/2/18 2:59
 * 发送验证码功能
 */
@WebServlet(name = "ValidationRegister", value = "/ValidationRegister")
public class ValidationRegister extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 响应参数格式设置
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        InputStreamReader insr = new InputStreamReader(req.getInputStream(), StandardCharsets.UTF_8);
        StringBuilder message = new StringBuilder();
        int respInt = insr.read();
        while (respInt != -1) {
            // 读取请求数据,将读取的字节流中的每一个字节转化为字符，然后添加到StringBuilder类型的对象中
            message.append((char) respInt);
            respInt = insr.read();
        }
        //将StringBuilder类型的对象message的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        Enroll enroll = JSON.parseObject(message.toString(), Enroll.class);
        System.out.println(enroll);
        String registerway=String.valueOf(enroll.getRegisterway());
        String useremail=String.valueOf(enroll.getUseremail());
        OpenDatabase judge=new OpenDatabase();
        if ("email".equals(registerway)){
        com.example.qianduan.changeclass.message exit=judge.searchdatabase(registerway,useremail);
        if(exit.getUser_name().equals("该邮箱未注册！")){
            String jsonobject= JSONObject.toJSONString("该邮箱未注册！");
            out.println(jsonobject);
        }else {
            String str="ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            StringBuilder sb=new StringBuilder(4);
            for(int i=0;i<4;i++)
            {char ch=str.charAt(new Random().nextInt(str.length()));sb.append(ch);}
            MailTest mailTest = new MailTest();
            try {mailTest.mail(useremail, sb.toString());
            } catch (MessagingException e) {
                e.printStackTrace();}
            String jsonobject= JSONObject.toJSONString(sb);
            out.println(jsonobject);}
        }else {
            String jsonobject= JSONObject.toJSONString("registerway错误！");
            out.println(jsonobject);
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
    }