package com.example.qianduan.servlet;

import com.alibaba.fastjson.JSONObject;
import com.example.qianduan.changeclass.ReturnSearch;
import com.example.qianduan.changeclass.Search;
import com.example.qianduan.jsonchangeclass.JsonChangeSearch;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 管理员查询注册用户功能
 * @author:石烨
 */
public class ControlSearch extends HttpServlet {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/demo?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS ="123456";
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection conn = null;
        Statement stmt = null;
        // 响应参数格式设置
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        //将此字符串对象输出
        PrintWriter out=resp.getWriter();
        //使用InputStreamReader对象，获取前端传来的数据.其中
        // request.getInputStream()是读取前端传递来的数据字节流，
        // StandardCharsets.UTF_8是将前端传来的数据转化为UTF-8的编码方式
        InputStreamReader insr = new InputStreamReader(req.getInputStream(), StandardCharsets.UTF_8);
        StringBuilder body = new StringBuilder();
        int respInt = insr.read();
        while(respInt!=-1) { // 读取请求数据
            //将读取的字节流中的每一个字节转化为字符，然后添加到StringBuilder类型的对象中
            body.append((char) respInt);
            respInt = insr.read();
        }
        //out的print方法可以输出对象
         //out.print(body);
        JsonChangeSearch jsonChangeSearch = new JsonChangeSearch();
        //将StringBuilder类型的对象的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        Search search = jsonChangeSearch.JsonChangeJavaObject(body.toString());
        List<ReturnSearch> list=new ArrayList<ReturnSearch>();
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            System.out.println("连接数据库...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 查询用户信息
            System.out.println("正在查询...");
            stmt = conn.createStatement();
            if (search.getFindWay().equals("name")){
            String sql = "SELECT * FROM user WHERE name='"+search.getInputcontent()+"'";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()){
                String user_name = rs.getString("user_name");
                String password = rs.getString("password");
                String phone_number = rs.getString("phone_number");
                String QQ_number = rs.getString("QQ_number");
                String email = rs.getString("email");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                String nper = rs.getString("nper");
                String direction = rs.getString("direction");
                String student_id = rs.getString("student_id");
                String professional_class = rs.getString("professional_class");
                String campus = rs.getString("campus");
                Date registration_date = rs.getDate("registration_date");
                int overdue_number = rs.getInt("overdue_number");
                String administrator = rs.getString("administrator");
                String borrow_status = rs.getString("borrow_status");
                list.add(new ReturnSearch(user_name,password,phone_number,QQ_number,email,name,sex,nper,direction,student_id,professional_class,campus,registration_date,overdue_number,administrator,borrow_status));
            }
                System.out.println("已找到");
            }
            if (search.getFindWay().equals("direction")){
                String sql = "SELECT * FROM user WHERE direction='"+search.getInputcontent()+"'";
                ResultSet rs = stmt.executeQuery(sql);
                while(rs.next()){
                    String user_name = rs.getString("user_name");
                    String password = rs.getString("password");
                    String phone_number = rs.getString("phone_number");
                    String QQ_number = rs.getString("QQ_number");
                    String email = rs.getString("email");
                    String name = rs.getString("name");
                    String sex = rs.getString("sex");
                    String nper = rs.getString("nper");
                    String direction = rs.getString("direction");
                    String student_id = rs.getString("student_id");
                    String professional_class = rs.getString("professional_class");
                    String campus = rs.getString("campus");
                    Date registration_date = rs.getDate("registration_date");
                    int overdue_number = rs.getInt("overdue_number");
                    String administrator = rs.getString("administrator");
                    String borrow_status = rs.getString("borrow_status");
                    list.add(new ReturnSearch(user_name,password,phone_number,QQ_number,email,name,sex,nper,direction,student_id,professional_class,campus,registration_date,overdue_number,administrator,borrow_status));
                }
            }
            if (search.getFindWay().equals("professional_class")){
                String sql = "SELECT * FROM user WHERE professional_class='"+search.getInputcontent()+"'";
                ResultSet rs = stmt.executeQuery(sql);
                while(rs.next()){
                    String user_name = rs.getString("user_name");
                    String password = rs.getString("password");
                    String phone_number = rs.getString("phone_number");
                    String QQ_number = rs.getString("QQ_number");
                    String email = rs.getString("email");
                    String name = rs.getString("name");
                    String sex = rs.getString("sex");
                    String nper = rs.getString("nper");
                    String direction = rs.getString("direction");
                    String student_id = rs.getString("student_id");
                    String professional_class = rs.getString("professional_class");
                    String campus = rs.getString("campus");
                    Date registration_date = rs.getDate("registration_date");
                    int overdue_number = rs.getInt("overdue_number");
                    String administrator = rs.getString("administrator");
                    String borrow_status = rs.getString("borrow_status");
                    list.add(new ReturnSearch(user_name,password,phone_number,QQ_number,email,name,sex,nper,direction,student_id,professional_class,campus,registration_date,overdue_number,administrator,borrow_status));
                }
            }
            if (search.getFindWay().equals("campus")){
                String sql = "SELECT * FROM user WHERE campus='"+search.getInputcontent()+"'";
                ResultSet rs = stmt.executeQuery(sql);
                while(rs.next()){
                    String user_name = rs.getString("user_name");
                    String password = rs.getString("password");
                    String phone_number = rs.getString("phone_number");
                    String QQ_number = rs.getString("QQ_number");
                    String email = rs.getString("email");
                    String name = rs.getString("name");
                    String sex = rs.getString("sex");
                    String nper = rs.getString("nper");
                    String direction = rs.getString("direction");
                    String student_id = rs.getString("student_id");
                    String professional_class = rs.getString("professional_class");
                    String campus = rs.getString("campus");
                    Date registration_date = rs.getDate("registration_date");
                    int overdue_number = rs.getInt("overdue_number");
                    String administrator = rs.getString("administrator");
                    String borrow_status = rs.getString("borrow_status");
                    list.add(new ReturnSearch(user_name,password,phone_number,QQ_number,email,name,sex,nper,direction,student_id,professional_class,campus,registration_date,overdue_number,administrator,borrow_status));
                }
            }
            if (search.getFindWay().equals("nper")){
                String sql = "SELECT * FROM user WHERE nper='"+search.getInputcontent()+"'";
                ResultSet rs = stmt.executeQuery(sql);
                while(rs.next()){
                    String user_name = rs.getString("user_name");
                    String password = rs.getString("password");
                    String phone_number = rs.getString("phone_number");
                    String QQ_number = rs.getString("QQ_number");
                    String email = rs.getString("email");
                    String name = rs.getString("name");
                    String sex = rs.getString("sex");
                    String nper = rs.getString("nper");
                    String direction = rs.getString("direction");
                    String student_id = rs.getString("student_id");
                    String professional_class = rs.getString("professional_class");
                    String campus = rs.getString("campus");
                    Date registration_date = rs.getDate("registration_date");
                    int overdue_number = rs.getInt("overdue_number");
                    String administrator = rs.getString("administrator");
                    String borrow_status = rs.getString("borrow_status");
                    list.add(new ReturnSearch(user_name,password,phone_number,QQ_number,email,name,sex,nper,direction,student_id,professional_class,campus,registration_date,overdue_number,administrator,borrow_status));
                }
            }
            if (search.getFindWay().equals("sex")){
                String sql = "SELECT * FROM user WHERE sex='"+search.getInputcontent()+"'";
                ResultSet rs = stmt.executeQuery(sql);
                while(rs.next()){
                    String user_name = rs.getString("user_name");
                    String password = rs.getString("password");
                    String phone_number = rs.getString("phone_number");
                    String QQ_number = rs.getString("QQ_number");
                    String email = rs.getString("email");
                    String name = rs.getString("name");
                    String sex = rs.getString("sex");
                    String nper = rs.getString("nper");
                    String direction = rs.getString("direction");
                    String student_id = rs.getString("student_id");
                    String professional_class = rs.getString("professional_class");
                    String campus = rs.getString("campus");
                    Date registration_date = rs.getDate("registration_date");
                    int overdue_number = rs.getInt("overdue_number");
                    String administrator = rs.getString("administrator");
                    String borrow_status = rs.getString("borrow_status");
                    list.add(new ReturnSearch(user_name,password,phone_number,QQ_number,email,name,sex,nper,direction,student_id,professional_class,campus,registration_date,overdue_number,administrator,borrow_status));
                }
            }
            String jsonObject= JSONObject.toJSONString(list);
            out.println(jsonObject);
            if (list.size()==0){
                out.print("失败");
            }
            System.out.println(list);
            //录入完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally {
            // 关闭资源
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
