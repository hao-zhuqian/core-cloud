package com.example.qianduan.changeclass;

import java.util.Date;

public class ReturnControl {
    private String InitiateTime;
    private String book;
    private int time;
    private String id;
    private String InitiateType;
    private int number;

    public ReturnControl() {
    }

    public ReturnControl(String initiateTime, String book, int time, String id, String initiateType, int number) {
        InitiateTime = initiateTime;
        this.book = book;
        this.time = time;
        this.id = id;
        InitiateType = initiateType;
        this.number = number;
    }

    public String getInitiateTime() {
        return InitiateTime;
    }

    public void setInitiateTime(String initiateTime) {
        InitiateTime = initiateTime;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInitiateType() {
        return InitiateType;
    }

    public void setInitiateType(String initiateType) {
        InitiateType = initiateType;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "InitiateTime=" + InitiateTime +
                ", book='" + book + '\'' +
                ", time=" + time +
                ", id='" + id + '\'' +
                ", InitiateType='" + InitiateType + '\'' +
                ", number=" + number;
    }
}
