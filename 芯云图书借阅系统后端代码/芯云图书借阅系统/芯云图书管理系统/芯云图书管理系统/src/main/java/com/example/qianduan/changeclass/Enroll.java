package com.example.qianduan.changeclass;

/**
 * @Author 刘媛媛
 * @create 2022/2/18 3:13
 */
public class Enroll {
    private String registerway;
    private String useremail;

    public String getRegisterway() {
        return registerway;
    }

    public void setRegisterway(String registerway) {
        this.registerway = registerway;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    @Override
    public String toString() {
        return "Enroll{" +
                "registerway='" + registerway + '\'' +
                ", useremail='" + useremail + '\'' +
                '}';
    }
}
