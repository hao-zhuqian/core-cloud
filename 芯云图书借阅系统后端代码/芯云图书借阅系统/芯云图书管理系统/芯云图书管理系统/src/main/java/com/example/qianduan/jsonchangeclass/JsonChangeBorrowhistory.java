package com.example.qianduan.jsonchangeclass;

import com.alibaba.fastjson.JSON;
import com.example.qianduan.changeclass.Addbook;
import com.example.qianduan.changeclass.BorrowhistoryClass;

public class JsonChangeBorrowhistory {
    public BorrowhistoryClass JsonChangeJavaObject(String str) {
        BorrowhistoryClass borrowhistoryClass = JSON.parseObject(str, BorrowhistoryClass.class);
        return borrowhistoryClass;
    }
}
