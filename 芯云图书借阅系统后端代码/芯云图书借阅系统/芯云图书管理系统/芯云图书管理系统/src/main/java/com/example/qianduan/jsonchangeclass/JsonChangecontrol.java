package com.example.qianduan.jsonchangeclass;

import com.alibaba.fastjson.JSON;
import com.example.qianduan.changeclass.Addbook;
import com.example.qianduan.changeclass.ControlClass;

public class JsonChangecontrol {
    public ControlClass JsonChangeJavaObject(String str) {
        ControlClass controlClass = JSON.parseObject(str,ControlClass.class);
        return controlClass;
    }
}
