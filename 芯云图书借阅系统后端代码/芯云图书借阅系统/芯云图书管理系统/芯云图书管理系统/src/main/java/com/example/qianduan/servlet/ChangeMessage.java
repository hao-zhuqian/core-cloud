package com.example.qianduan.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.qianduan.method.ChangeDatabase;
import com.example.qianduan.changeclass.Change;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

/**
 * 修改个人信息功能
 * @author：刘媛媛
 */
@WebServlet(name = "ChangeMessage", value = "/ChangeMessage")
public class ChangeMessage extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 响应参数格式设置
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        InputStreamReader insr = new InputStreamReader(req.getInputStream(), StandardCharsets.UTF_8);
        StringBuilder message = new StringBuilder();
        int respInt = insr.read();
        while (respInt != -1) {
            // 读取请求数据,将读取的字节流中的每一个字节转化为字符，然后添加到StringBuilder类型的对象中
            message.append((char) respInt);
            respInt = insr.read();
        }
        //将StringBuilder类型的对象message的对象通过toString方法转化为String类型，然后用fastjson的json包进行转化
        Change change = JSON.parseObject(message.toString(),Change.class);
        System.out.println(change);
        String useclass=String.valueOf(change.getUseclass());
        String usemessage=String.valueOf(change.getUsemessage());
        String changeclass=String.valueOf(change.getChangeclass());
        String changemessage =String.valueOf(change.getChangemessage());
        ChangeDatabase changeDatabase=new ChangeDatabase();
        String result=changeDatabase.writedatabase(useclass,usemessage,changeclass,changemessage);
        String jsonobject= JSONObject.toJSONString(result);
        out.println(jsonobject);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}