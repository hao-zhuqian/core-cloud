package com.example.qianduan.changeclass;

public class UserbookClass {
    private String FindWays;
    private String input;

    public UserbookClass() {
    }

    public String getFindWays() {
        return FindWays;
    }

    public void setFindWays(String findWays) {
        FindWays = findWays;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    @Override
    public String toString() {
        return "UserbookClass{" +
                "FindWays='" + FindWays + '\'' +
                ", input='" + input + '\'' +
                '}';
    }
}
