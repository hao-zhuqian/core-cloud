package com.example.qianduan.changeclass;

/**
 * @Author 刘媛媛
 * @create 2022/2/18 3:22
 */
public class Change {
    private String useclass;
    private String usemessage;
    private String changeclass;
    private String changemessage;

    public String getUseclass() {
        return useclass;
    }

    public void setUseclass(String useclass) {
        this.useclass = useclass;
    }

    public String getUsemessage() {
        return usemessage;
    }

    public void setUsemessage(String usemessage) {
        this.usemessage = usemessage;
    }

    public String getChangeclass() {
        return changeclass;
    }

    public void setChangeclass(String changeclass) {
        this.changeclass = changeclass;
    }

    public String getChangemessage() {
        return changemessage;
    }

    public void setChangemessage(String changemessage) {
        this.changemessage = changemessage;
    }

    @Override
    public String toString() {
        return "Change{" +
                "useclass='" + useclass + '\'' +
                ", usemessage='" + usemessage + '\'' +
                ", changeclass='" + changeclass + '\'' +
                ", changemessage='" + changemessage + '\'' +
                '}';
    }
}
