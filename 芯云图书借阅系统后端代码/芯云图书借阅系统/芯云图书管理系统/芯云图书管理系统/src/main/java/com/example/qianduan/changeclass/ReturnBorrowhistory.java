package com.example.qianduan.changeclass;

public class ReturnBorrowhistory {
    private int Book_id;
    private String book;
    private String InitiateTime;
    private int Time;
    private String state;

    public ReturnBorrowhistory() {
    }

    public ReturnBorrowhistory(int book_id, String book, String initiateTime, int time, String state) {
        Book_id = book_id;
        this.book = book;
        InitiateTime = initiateTime;
        Time = time;
        this.state = state;
    }

    public int getBook_id() {
        return Book_id;
    }

    public void setBook_id(int book_id) {
        Book_id = book_id;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getInitiateTime() {
        return InitiateTime;
    }

    public void setInitiateTime(String initiateTime) {
        InitiateTime = initiateTime;
    }

    public int getTime() {
        return Time;
    }

    public void setTime(int time) {
        Time = time;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "book='" + book + '\'' +
                ", InitiateTime='" + InitiateTime + '\'' +
                ", Time='" + Time + '\'' +
                ", state='" + state + '\'';
    }
}
